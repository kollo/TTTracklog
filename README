


                    TTTracklog and log2itn

                       VERSION 1.14

            (C) 2007-2010 by Markus Hoffmann
              (kollo@users.sourceforge.net)

Name        : TTTracklog / log2itn
Version     : 1.14                              Vendor: Markus Hoffmann
Group       : Embedded/Shell                    License: GPL
Packager    : Markus Hoffmann <kollo@users.sourceforge.net>
URL         : http://www.opentom.org/TTTracklog
URL         : http://www.opentom.org/log2itn
Summary     : A GPS-rawdata/gpx track logging daemon
Description :

TTTracklog is a tracklogger which is independent and uses minimal resources. The
tracks are outputted as NMEA log, gpx files and .itn route files.

To make the program simple and not consuming noticeable processing time on the
TomTom it is designed to run completely in the background and will not be
switched on or off. It simply logs all tracks and nmea-sentences in the three
files: 

itn/20xx-xx-xx.itn             The track for the route menu
statdata/nmea-20xx-xx-xx.log   The nmea raw messages
statdata/20xx-xx-xx.gpx        The Track in gpx format

where 20xx-xx-xx is the actual date. The new log files automatically will be
created every day. This prevents the files being split if used during midnight.
The .log files can be easily merged togeter with cat.

The .gpx files can be uploaded to the http://www.openstreetmap.org/ project.

The .itn files can be directly used on the TomTom to find positions of the last
stop etc. (Use the route management function).

The Files can become as large as 20 MBytes each, containing up to approx. 130000
lines of log output which is about 43000 Trackpoints (if the tomtom is on 24h,
recorded every two seconds). It is important to (re)move them before the SDCARD
or internal FLASH memory is full. 


INSTALLATION:
=============

Unpack the binary package (zip file) and copy the content to your TomTom (empty
directories included). If the directories already exist on your TomTom, you have
to paste the new files into these directories. 

d    bin/
d    itn/
-    README
-    RELEASE_NOTES
d    SDKRegistry/
d    statdata/
d    text/

Make sure, that these directories exist. They might be already there, 
which is fine. In that case make sure that the contents of bin/ and SDKRegistry/
get to the TomTom. 

USAGE:
======

Start:
You need to start the tracklogging by activation the Tracklog Icon in your 
TomTom menu. You should get a message, that TTTracklog has been started. 
From now on up to a reboot or a reset of the Device (which is also 
performed in case you connect the TomTom to your computer), the GPS track 
is logged and written to the files described above. If you are not sure, if 
TTTracklog is already running and you want to activate it, it is save to start 
it again from the icon menu. 

Stop:
There are several ways to stop tracklogging:

* reboot the device (use the reset function)
* use an extension which adds a stop button to TTTracklog, see Bernards 
  contribution on the discussion page.
* connect the device to a computer
* If you have installed TTconsole, you can stop it by typing: 

  killall TTTracklog

You can create a script to do this, if you want.

How it works:
-------------
By default, TTTracklog saves:

* in the .itn file: one trackpoint every 20 minutes, and one point every time 
  you stop more than 29 seconds
* in the .gpx file: one trackpoint every second 


Maintainance:
=============
From Time to time you should remove the files

  text/TTTracklog-20*.txt 
  statdata/nmea-20*.log   and
  statdata/20*.gpx 

from the device to prevent it from consuming all the memory. 

But the memory will at least last for 10 Days of 24 hours permanent 
tracklogging. The itn files do not take much memory, so you can collect 
thousands of them without problems. 

The itn files store the positions of all rests which last more than 30 seconds. 
Every 20 Minutes a point is stored as well. So you can reconstruct where you 
have been by selecting the route managment icon, then options, then load a route.
The route of the actual day will be updated roughly every 10 Minutes. So you can 
easily navigate back to where you have been before. 

COMMENTS:
=========

In addition there is a logfile created:

  text/TTTracklog-JJJJ-MM-DD.txt

This file can be viewed with the document viewer and contains information on 
the data processing of TTTracklog. This is normally not interesting to users. 
Only in case of strange behaviour (which is extremly seldom) and for debugging,
this file becomes useful. So if you encounter problems with the application, 
you might send me this output. 


whereami
========

We now provide another tool, which you will find in the bin/ foler:
whereami is a tool which allows to get information about a specified 
location from the TomTom Device. 

e.g.
whereami --lon 53.1234 --lat 9.1234

will output the Streetname and City name of the Location closest to the 
specified point. If you do not provide the coordinates, per default the 
actual position will be used if it could be acheived. 

TTTracklog can run without this tool. (it does not use it).

This tool is useful if you want to convert the position recorded into 
the .itn file (or any other waypoint like dataset) into adresses. But you 
will have to write your own script to do so.

log2itn
=======
log2itn is a converter (filter) for NMEA log files. 
It converts them into .gpx and/or .itn files according to divers parameters. 
It also shows the summaries about your saved tracks.

Example of use:

I make a nice tour. Back at home, I realize that I have set wrong parameters 
in the TTTracklog-wrapper commandline and i'm not satisfied with my .itn and 
.gpx files. Using log2itn gives me the possibility to generate new files like 
I want.

TTTracklog can run without this tool. (it does not use it).
log2itn is available for linux on intel/PC (ubuntu,debian) and 
on WINDOWS-XP.

for a list of commandline options run log2itn -h

For further instructions please read the homepage:

http://www.opentom.org/Log2itn


COPYRIGHT
=========

 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details

Please read the file INSTALL for compiling instructions.

The Software is not tested on all TomTom devices, 
but it might work.


Some procedures are taken from the X11-Basic package, some algorithms 
taken from GPS-Earth.

SOURCE CODE
===========
You can download the sourcecode package for TTTracklog, log2itn and whereami 
at http://www.opentom.org/TTTracklog .


BUG REPORTS       

If you find a bug in the TTTracklog, you should report it. But first, you should
make sure that it really is a bug, and that it appears in the latest version of
the TTTracklog package that you have.

Once you have determined that a bug actually exists, mail a
bug report to kollo@users.sourceforge.net. If you have a fix,
you are welcome to mail that as well! Suggestions may 
be mailed to the X11-Basic mailing list on 
www.sourceforge.net/projects/x11-basic  or posted to the bug
tracking system.

Comments and  bug  reports  concerning  this  manual  page
should be directed to kollo@users.sourceforge.net.


AUTHOR

Markus Hoffmann <kollo@users.sourceforge.net>

ACKNOWLEDGEMENTS

Many thanks to Bernard (gpstour.ch) for many good ideas, for beta-testing and 
for the extension package.

COPYRIGHT

Copyright (C) 2008-2009 Markus Hoffmann <kollo@users.sourceforge.net>

This program is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation;
either version 2 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more
details.
