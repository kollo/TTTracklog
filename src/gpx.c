/* gpx.c                            (c) Markus Hoffmann  2007-2009
*/

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */


/*
<gpx version="1.1 [1] ?" creator="xsd:string [1] ?"> 
  <metadata> metadataType </metadata>  
  <wpt> wptType </wpt>  
  <rte> rteType </rte>  
  <trk> trkType </trk>  
  <extensions> extensionsType </extensions> [0..1] ?
</gpx>
*/


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

#include "TTTracklog.h"
#include "nmea.h"
#include "gpx.h"
#include "geo.h"

extern TRACKPOINT *track;
extern int anztrack;
extern int verbose;

/* Trackststistik */
TOUR stati;
TOUR tour[MAX_TOUR];
int anztour=0;

double gpxtime,gpxdate;

double gpx_trackptsep=0;  /* in seconds */
double gpx_trackptsep2=60*60*24;  /* in seconds */
double gpx_trackptdist=0;  /* in meters */
double gpx_trackptdist2=100*1000;  /* in meters */
double gpx_trackptdeg=0;  /* in degrees */
double gpx_trackptdeg2=360;  /* in degrees */
int gpx_name_flags=0;     /* naming of trackpoints */
int do_gpxwp=0;

int gpx_flags=GPX_SAT|GPX_DOP;          /* pattern of which data goes to gpx */
int gpx_halttime=2;
int dodecimate=0;
double distance_significance=5;  /* in Meters */

void gpx_init(FILE *dptr) {
  fprintf(dptr,"<?xml version='1.0' encoding='ISO-8859-1' standalone='yes'?>\n"
               "<gpx version='1.1' creator='TTTracklog V." TTTracklogVERSION "' " 
               "xmlns='http://www.topografix.com/GPX/1/1' "
               "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' "
               "xsi:schemaLocation='http://www.topografix.com/GPX/1/1 "
               "http://www.topografix.com/GPX/1/1/gpx.xsd'>\n"
               "\n<metadata>\n"
               "  <link href=\"http://www.opentom.org/TTTracklog\">\n"
               "    <text>TTTracklog (c) 2008-2009 by Markus Hoffmann</text>\n"
               "  </link>\n"
               "  <time>%s</time>\n"
               "  <bounds maxlat=\"%.10g\" maxlon=\"%.10g\" minlat=\"%.10g\" minlon=\"%.10g\"/>\n"
               "</metadata>\n\n",gpx_time(gpxtime,gpxdate),stati.maxlat,stati.maxlon,
	       stati.minlat,stati.minlon);
}
void gpx_tracksep(FILE *dptr) {
  fprintf(dptr,"</trkseg><trkseg>\n");
}
void gpx_close(FILE *dptr) {
  fprintf(dptr,"</gpx>\n");
  fclose(dptr);
}
/*
<xxx                            
 lat="latitudeType"             (Breite)
 lon="longitudeType">           (L�nge)
 <ele> xsd:decimal </ele>       (H�he)
 <time> xsd:dateTime </time>    (Datum und Zeit)
 <magvar> degreesType </magvar> (lokale magn. Missweisung)
 <geoidheight> xsd:decimal </geoidheight>     (H�he bezogen auf Geoid)
 <name> xsd:string </name>      (Beschreibung)
 <cmt> xsd:string </cmt>        (Kommentar)
 <desc> xsd:string </desc>      (Elementbeschreibung)
 <src> xsd:string </src>        (Quelle)
 <link> linkType </link>        (Link)
 <sym> xsd:string </sym>        (Darstellungssymbol)
 <type> xsd:string </type>      (Klassifikation)
 <fix> fixType </fix> 
 <sat> xsd:nonNegativeInteger </sat>          (Anzahl der empfangenen Satelliten)
 <hdop> xsd:decimal </hdop>     (hDOP)
 <vdop> xsd:decimal </vdop>     (vDOP)
 <pdop> xsd:decimal </pdop>     (3D DOP)
 <ageofdgpsdata> xsd:decimal </ageofdgpsdata> (Letzter DGPS update)
 <dgpsid> dgpsStationType </dgpsid>           (DGPS ID)
 <extensions> extensionsType </extensions>    (GPX Erweiterung)
</xxx>                      

*/


char *gpx_time(double t,double d) {
  static char buf[24];
  int h,m,s;
  int J,M,D;

  h=(int)(t/10000);t-=h*10000;	   
  m=(int)(t/100);t-=m*100;
  s=(int)t;
  D=(int)(d/10000);d-=D*10000;
  M=(int)(d/100);d-=M*100;
  J=2000+(int)d;
  sprintf(buf,"%04d-%02d-%02dT%02d:%02d:%02dZ",J,M,D,h,m,s);  
  return(buf);
}

void get_stats() {
  get_bounds(0,anztrack,&stati); /* Gesamtstatistik */
  get_tours();
}

/* Bereche einzelne Tracks/Touren 
   Rueckgabe ist die Anzahl der gefundenen Touren 
   */

int get_tours() {
  int i=1;
  anztour=0;
  tour[anztour].start=0;
  while(i<anztrack) {
    if((track[i].flags&FLAG_PGLOR) && anztour<MAX_TOUR-1) {
      tour[anztour].stop=i-1;
      get_bounds(tour[anztour].start,tour[anztour].stop,&tour[anztour]);
      anztour++;
      tour[anztour].start=i;
    }
    i++;
  }
  tour[anztour].stop=anztrack;
  get_bounds(tour[anztour].start,tour[anztour].stop,&tour[anztour]);
  anztour++;
  return(anztour);
}


/* Berechne Track-Statistik fuer einen Teil (oder die Ganze) Strecke */

void get_bounds(int starttrack,int anztrack,TOUR *stati) {
  int i,k;
  i=starttrack;
  stati->maxlon=stati->maxlat=-360;
  stati->maxalt=-4711;
  stati->maxtim=stati->maxspeed=0;
  stati->minlon=stati->minlat=stati->minalt=99999;
  stati->mintim=(double)0xffffffff;
  stati->totdist=stati->standzeit=stati->wegzeit=0;
  stati->validtrack=0;
  stati->anztrack=anztrack-starttrack;

 // printf("GET_BOUNDS: %d %d\n",starttrack,anztrack);


  /* Startpunkt (= ersten gueltigen) finden */
  while((track[i].quality==0 || track[i].timestamp<24*60*60.0) && i<anztrack) i++; 
  gpxtime=track[i].time;
  gpxdate=track[i].date;
  stati->start=k=i;
  while(i<anztrack) {
    stati->maxspeed=max(stati->maxspeed,track[i].speed);
    stati->maxalt=max(stati->maxalt,track[i].alt);
    stati->minalt=min(stati->minalt,track[i].alt);
    stati->maxlon=max(stati->maxlon,track[i].lon);
    stati->minlon=min(stati->minlon,track[i].lon);
    stati->maxlat=max(stati->maxlat,track[i].lat);
    stati->minlat=min(stati->minlat,track[i].lat);
    stati->maxtim=max(stati->maxtim,track[i].timestamp);
    stati->mintim=min(stati->mintim,track[i].timestamp);
   // printf("k=%d/%d\n",i,anztrack);
    k=i++;
    stati->validtrack++;
    /* Vorspulen, bis Geschwindigkeit >0 */
    while((track[i].speed==0 || track[i].quality==0 || track[i].timestamp<24*60*60.0) && i<anztrack) i++;
    if(i>=anztrack) break; 
    if(i-k>1) {
     //   printf("--> Vorgespult auf %d\n",i);
        stati->standzeit+=track[i].timestamp-track[k].timestamp;
    } else {
        stati->wegzeit+=track[i].timestamp-track[k].timestamp;
        stati->totdist+=distance(track[i].lon,track[i].lat,track[k].lon,track[k].lat);
    }
  }
  /* Jetzt das Ende finden */
  while((track[i].quality==0 || track[i].timestamp<24*60*60.0) && i>starttrack) i--; 
//  printf("Rueckwaerts das Ende: %d\n",i);
  if(i>k) {  /* Hier noch die Position und Orts-Grenzen auffrischen mit letztem gueltigen Punkt */
    stati->standzeit+=track[i].timestamp-track[k].timestamp;
  }
  if(i>=stati->start) {
    stati->maxalt=max(stati->maxalt,track[i].alt);
    stati->minalt=min(stati->minalt,track[i].alt);
    stati->maxlon=max(stati->maxlon,track[i].lon);
    stati->minlon=min(stati->minlon,track[i].lon);
    stati->maxlat=max(stati->maxlat,track[i].lat);
    stati->minlat=min(stati->minlat,track[i].lat);
    stati->maxtim=max(stati->maxtim,track[i].timestamp);
    stati->mintim=min(stati->mintim,track[i].timestamp);
    stati->stop=i;
  } else stati->stop=stati->start;
}


#define EARTHRADIUS 6371000

double orthodist(int a,int b,int c) {
  double ax,ay,bx,by,cx,cy;
  double ff;

/* Dies ist nur eine Approximative norm, funktioniert nur bei kleinen
   Abstaenden ! */
   
  ay=track[a].lat*DEG2RAD;
  ax=track[a].lon*DEG2RAD*cos(ay);
  by=track[b].lat*DEG2RAD;
  bx=track[b].lon*DEG2RAD*cos(by)-ax;
  cy=track[c].lat*DEG2RAD;
  cx=track[c].lon*DEG2RAD*cos(cy)-ax;
  
  by-=ay;
  cy-=ay;

  ff=bx*cy-cx*by;
  if(ff<0) ff=-ff;

  ff/=sqrt(bx*bx+by*by);
  return(EARTHRADIUS*ff);
}


/* Douglas-Peucker-Algorithmus */
int decimate(int *liste,int anzlist) {
  int mi=-1;
  double md=0,d;
  int i,j;
  static int r=0;
  
//  printf("r=%d\n",r);
 // for(i=0;i<r;i++) printf(" ");
//  printf("decimate: [%d:%d] (%d) ",liste[0],liste[anzlist-1],anzlist);
  
  /* Erst die Marker entfernen */
  for(j=i=0;i<anzlist;i++) {
    liste[j]=liste[i];
    if(liste[j]>=0) j++;
  }
  anzlist=j;

  if(anzlist==1) {
    liste[1]=liste[0];
//    printf(" done\n");return(2);
  }
  
  if(anzlist<=2) {
//    printf(" done\n");
    return(anzlist);
  }
  r++;
  /* Berechne maximalen Abstand von Grundlinie */
  for(i=1;i<anzlist-1;i++) {
    d=orthodist(liste[0],liste[anzlist-1],liste[i]);
    if(d>md) {md=d; mi=i;}
  }
//  printf("Maxdist=%g, %d\n",md,mi);
  /* wenn groesser MAXDIST */
  if(md>distance_significance) {
  /* rufe decimate mit beiden Teilrouten auf und kombiniere das Ergebnis */
    int liste2[anzlist];
    memcpy(liste2,&liste[mi],(anzlist-mi)*sizeof(int));
    i=decimate(liste,mi);
    j=decimate(liste2,anzlist-mi);
    memcpy(&liste[i-1],liste2,j*sizeof(int));
    r--;
    return(i+j-1);
  } else {
    /* wenn kleiner MAXDIST */
    liste[1]=liste[anzlist-1];
    r--;
    return(2);
  }
}

extern int get_waypoints(int starttrack,int anztrack,int *liste);
void save_gpxfile(char *gpxfilename) {
  FILE *dptr;
  char name[256];
  int count=0,wptcount=0;
    
  dptr=fopen(gpxfilename,"w");
  if(!dptr) {
    printf("Failed to open gpx file %s.\n",gpxfilename);
    return;
  }
  if(verbose>0) printf("--> %s [",gpxfilename);
  gpx_init(dptr);  
  if(do_gpxwp) {
    int liste[anztrack];
    int anzlist;
    anzlist=get_waypoints(0,anztrack,liste);
    wptcount=save_gpxwaypoints(dptr,liste,anzlist);
  }
  if(anztour>1) {
    int i;
    for(i=0;i<anztour;i++) {
      if(tour[i].validtrack) {
        sprintf(name,"TomTom TOUR LOG %6.5g km (%d/%d)",tour[i].totdist,tour[i].validtrack,tour[i].anztrack);
        count+=save_gpxtrack(dptr,i,name,tour[i].start,tour[i].stop);
      }
    }
  } else {
    sprintf(name,"TomTom ACTIVE LOG (%d/%d)",stati.validtrack,stati.anztrack);
    count=save_gpxtrack(dptr,-1,name,0,anztrack);
  }
  gpx_close(dptr);
  if(verbose>0) printf("] (%d waypoints and) %d/%d/%d valid trackpoints.\n",wptcount,count,stati.validtrack,stati.anztrack);
}


/*
lat="latitudeType [1] ?"
lon="longitudeType [1] ?">
<ele> xsd:decimal </ele> [0..1] ?
<time> xsd:dateTime </time> [0..1] ?
<magvar> degreesType </magvar> [0..1] ?
<geoidheight> xsd:decimal </geoidheight> [0..1] ?
<name> xsd:string </name> [0..1] ?
<cmt> xsd:string </cmt> [0..1] ?
<desc> xsd:string </desc> [0..1] ?
<src> xsd:string </src> [0..1] ?
<link> linkType </link> [0..*] ?
<sym> xsd:string </sym> [0..1] ?
<type> xsd:string </type> [0..1] ?
<fix> fixType </fix> [0..1] ?
<sat> xsd:nonNegativeInteger </sat> [0..1] ?
<hdop> xsd:decimal </hdop> [0..1] ?
<vdop> xsd:decimal </vdop> [0..1] ?
<pdop> xsd:decimal </pdop> [0..1] ?
<ageofdgpsdata> xsd:decimal </ageofdgpsdata> [0..1] ?
<dgpsid> dgpsStationType </dgpsid> [0..1] ?
<extensions> extensionsType </extensions> [0..1] ?

*/

extern char wptname[];
int save_gpxwaypoints(FILE *dptr,int *liste,int anzlist) {
  int j,i;
  for(j=0;j<anzlist;j++) {
    i=liste[j];
    fprintf(dptr,"<wpt lat='%.10g' lon='%.10g'><ele>%g</ele><time>%s</time>",
                 track[i].lat,track[i].lon,
		 track[i].alt,gpx_time(track[i].time,track[i].date));
      fprintf(dptr,"<name>%s</name>",&wptname[j*64]);
      fprintf(dptr,"<course>%g</course><speed>%g</speed>",track[i].kurs,track[i].speed*KNOTEN/3.6);
      if(gpx_flags&GPX_MAGVAR) fprintf(dptr,"<magvar>%g</magvar>",track[i].magdev);
      if(gpx_flags&GPX_DGPS)   fprintf(dptr,"<ageofdgpsdata>%g</ageofdgpsdata>",track[i].dgpsalter);
      if(gpx_flags&GPX_FIX)    fprintf(dptr,"<fix>%d</fix>",track[i].fix);
      if(gpx_flags&GPX_MODE1)  fprintf(dptr,"<mode>%c</mode>",track[i].mode);
      if(gpx_flags&GPX_DOP) {
        fprintf(dptr,"<pdop>%g</pdop>",track[i].pdop);
        fprintf(dptr,"<hdop>%g</hdop>",track[i].hdop);
        fprintf(dptr,"<vdop>%g</vdop>",track[i].vdop);
      }
      if(gpx_flags&GPX_MODE2) fprintf(dptr,"<mode2>%c</mode2>",track[i].mode2);
      if(gpx_flags&GPX_SAT) fprintf(dptr,"<sat>%d</sat>",(int)track[i].numsat);
    fprintf(dptr,"</wpt>\n");
  }
  fprintf(dptr,"\n");
  return j;
}

int save_gpxtrack_liste(FILE *dptr,int *liste,int anzlist) {
  int i,j;
  static int k=0;
  char *times;
  static double dist=0;
//  printf("Save Liste: a=%d n=%d\n",*liste,anzlist);
  for(j=0;j<anzlist;j++) {
    i=liste[j];
    if(i==-1) {gpx_tracksep(dptr); k=0;}
    else if(i==-2) {k=0;dist=0;}
    else {
      fprintf(dptr,"   <trkpt lat='%.10g' lon='%.10g'><ele>%g</ele><time>%s</time>",
                 track[i].lat,track[i].lon,
		 track[i].alt,gpx_time(track[i].time,track[i].date));
      if(gpx_name_flags) {   
        fprintf(dptr,"<name>");
        if(gpx_name_flags&1) {
          times=vs_time2(track[i].timestamp);
          fprintf(dptr,"%s",times);
	  free(times);
        } 
	if(gpx_name_flags&2 && k) {
          dist+=(distance(track[i].lon,track[i].lat,track[k].lon,track[k].lat)*1000);
          fprintf(dptr," %g km ",((double)((int)dist))/1000);
        }
	if(gpx_name_flags&4) {
          fprintf(dptr," #%d ",i);
        }
	fprintf(dptr,"</name>");
      }
      fprintf(dptr,"<course>%g</course><speed>%g</speed>",track[i].kurs,track[i].speed*KNOTEN/3.6);
      if(gpx_flags&GPX_MAGVAR) fprintf(dptr,"<magvar>%g</magvar>",track[i].magdev);
      if(gpx_flags&GPX_DGPS)   fprintf(dptr,"<ageofdgpsdata>%g</ageofdgpsdata>",track[i].dgpsalter);
      if(gpx_flags&GPX_FIX)    fprintf(dptr,"<fix>%d</fix>",track[i].fix);
      if(gpx_flags&GPX_MODE1)  fprintf(dptr,"<mode>%c</mode>",track[i].mode);
      if(gpx_flags&GPX_DOP) {
        fprintf(dptr,"<pdop>%g</pdop>",track[i].pdop);
        fprintf(dptr,"<hdop>%g</hdop>",track[i].hdop);
        fprintf(dptr,"<vdop>%g</vdop>",track[i].vdop);
      }
      if(gpx_flags&GPX_MODE2) fprintf(dptr,"<mode2>%c</mode2>",track[i].mode2);
      if(gpx_flags&GPX_SAT) fprintf(dptr,"<sat>%d</sat>",(int)track[i].numsat);
      fprintf(dptr,"</trkpt>\n");
      k=i;
    }
  }
  return(j);
}


int save_gpxtrack(FILE *dptr,int number, char *name,int starttrack,int anztrack) {
  int j,k=0,i=starttrack;
  double d,cd,dt;
  int liste[anztrack];
  int anzlist=0,count=0;
  
  fprintf(dptr,"<trk><name>%s</name>\n",name);
  if(number>=0) fprintf(dptr,"<number>%d</number>\n",number);
  fprintf(dptr,"<trkseg>\n");
  j=-2;save_gpxtrack_liste(dptr,&j,1); /* Initialisieren */
  
  /* Startpunkt finden */
  while((track[i].quality==0) && i<anztrack) i++; 
  while(i<anztrack) {
    if(k==0 
      || (dt=track[i].timestamp-track[k].timestamp)>=gpx_trackptsep2
      || (d=distance(track[i].lon,track[i].lat,track[k].lon,track[k].lat)*1000)>=gpx_trackptdist2
      || (cd=fabs(track[i].kurs-track[k].kurs))>=gpx_trackptdeg2
      || (dt>=gpx_trackptsep && d>=gpx_trackptdist && cd>=gpx_trackptdeg)) {
      liste[anzlist++]=i;
      count++;
      k=i;
    }
    j=i++;
    while((track[i].speed==0 || track[i].quality==0) && i<anztrack) i++;
    if(i>=anztrack) break; 
    if(i-j>gpx_halttime) liste[anzlist++]=-1; /* Tracksep */
  }
  /* Wenn --decimate, dann dezimiere die Liste jetzt ... */
  if(dodecimate) {
    count=anzlist=decimate(liste,anzlist);
  }
  save_gpxtrack_liste(dptr,liste,anzlist);
  fprintf(dptr,"</trkseg>\n"
               "</trk>\n");
  if(verbose>0) printf("  TRACK #%d %d/%d trackpoints.\n",number,count,anztrack-starttrack);
  return(count);
}

extern char *date;

void print_singlestat(FILE *sysdptr, TOUR stati) {
  char *d,*t;
  d=vs_date2((time_t)stati.mintim);
  t=vs_time2((time_t)stati.mintim);
  fprintf(sysdptr,"Period:      %s %s to ",d,t);
  free(d);free(t);
  d=vs_date2((time_t)stati.maxtim);
  t=vs_time2((time_t)stati.maxtim);
  fprintf(sysdptr,"%s %s\n",d,t);
  free(d);free(t);
  fprintf(sysdptr,"Region:      lon=[%g:%g]; lat=[%g:%g]\n",stati.minlon,
    stati.maxlon,stati.minlat,stati.maxlat);
  fprintf(sysdptr,"Altitude:    [%g:%g] m\n",stati.minalt,stati.maxalt);
  fprintf(sysdptr,"Distance:    %g km\n",stati.totdist);
  fprintf(sysdptr,"Driving time: %s\n",h2t(stati.wegzeit));
  fprintf(sysdptr,"Stay time:    %s\n",h2t(stati.standzeit));
  fprintf(sysdptr,"Maxspeed:    %g km/h\n",stati.maxspeed*KNOTEN);
  if(stati.wegzeit) fprintf(sysdptr,"Average:     %g km/h\n",stati.totdist/stati.wegzeit*60*60);
  if(stati.wegzeit+stati.standzeit) fprintf(sysdptr,"Tot. Average: %g km/h\n",stati.totdist/(stati.wegzeit+stati.standzeit)*60*60);
}

void print_statistik(FILE *sysdptr) {
  char *d,*t;
  int i;
  fprintf(sysdptr,"\nSummary:\n========\n"
                  "Trackpoints: [%d:%d] %d/%d\n",stati.start,stati.stop,stati.validtrack,stati.anztrack);
  fprintf(sysdptr,"Date:        %s\n",date);
  print_singlestat(sysdptr,stati);
  
  if(anztour>1) {
    fprintf(sysdptr,"\n%d Tours:\n",anztour);
    for(i=0;i<anztour;i++) {
      fprintf(sysdptr,"%d:%5d-%5d: ",i,tour[i].start,tour[i].stop);
      d=vs_date2((time_t)tour[i].mintim);
      t=vs_time2((time_t)tour[i].mintim);
      fprintf(sysdptr,"%s %s -- ",d,t);
      free(d);free(t);
      d=vs_date2((time_t)tour[i].maxtim);
      t=vs_time2((time_t)tour[i].maxtim);
      fprintf(sysdptr,"%s %s > ",d,t);
      free(d);free(t);
      fprintf(sysdptr,"%6.5g km\n",tour[i].totdist);
    }
    for(i=0;i<anztour;i++) {
      fprintf(sysdptr,"\nTour #%d:     [%d:%d] %d/%d\n",i,tour[i].start,tour[i].stop,tour[i].validtrack,tour[i].anztrack);
      print_singlestat(sysdptr,tour[i]);
    }
  }
  fprintf(sysdptr,"\n");
}
