/* itn module konvertiert nmea-log files in brauchbare itn Routen-Files fuer TomTom  */
/*(c) Markus Hoffmann 2008 */

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "TTTracklog.h"
#include "nmea.h"
#include "geo.h"
#include "itn.h"

extern TRACKPOINT *track;
extern int anztrack;
extern int verbose;

double itn_wayptsep=DEFAULT_WAYPTSEP;     /* minimal time between WPT in minutes */
double itn_wayptsep2=DEFAULT_WAYPTSEP2;   /* maximal time between WPT in minutes */
double itn_wayptdist=DEFAULT_WAYPTDIST;   /* in meters */
double itn_wayptdist2=DEFAULT_WAYPTDIST2; /* in meters */
double itn_wayptdeg=0;      /* in degrees */
double itn_wayptdeg2=360;   /* in degrees */
double itn_halttime=DEFAULT_HALTTIME;     /* in seconds */
int itn_localtime=1;     /* use localtime instead of UTC-00 */

/* Berechnet die Wegpunkte aus den Trackdaten */


char wptname[MAX_ANZWP*64];

/* Findet in einem Teil-Stueck des Tracks (beginnend mit starttrack der L"ange
   anztrack) Wegpunkte nach den spezifizierten Kritereien (wie itn Files) 
   
   Es gibt noch Probleme mit dem letzten Wegpunkt, falls der Track dort nicht
   g"ultig ist.
   */


int get_waypoints(int starttrack,int anztrack,int *liste) {
  int i=starttrack;  /* Index fuer Trackpunkte */
  int anzwp=0;
  int k;             /* Index des letzten Wegpunkts */
  int j;             /* Letzter Trackpunkt */
  int flag;
  double d,cd,dt;

  anztrack+=i;
  
  /* Suche ersten brauchbaren Trackpunkt und verwende ihn als ersten (start) 
     Wegpunkt */
  
  while((track[i].quality==0 || track[i].timestamp<24*60*60) && i<anztrack) i++;
  if(i==anztrack) return(anzwp); /* Dann sind ueberhaupt keine brauchbaren Punkte vorhanden */
  sprintf(&wptname[anzwp*64],"START (#%d)",i);
  liste[anzwp]=i;
  anzwp++;
  k=i;
  
  for(j=i;i<anztrack && anzwp<MAX_ANZWP-1;j=i++) {
    /* Vorspulen auf naechsten brauchbaren Trackpunkt */
    flag=0;
    while((track[i].speed==0 || track[i].quality==0 || track[i].timestamp<24*60*60.0) && i<anztrack-1) {
      i++;
      if(verbose>2) printf(".");
      flag=1;
    }

    if(flag) { /* Wenn vorgespult worden ist, dann
                  ueberpruefe, ob das als Pause gewertet werden kann.*/
      if(track[i].timestamp-track[j].timestamp>itn_halttime) {

        if((track[i].timestamp-track[j].timestamp)>100*60) {
	  sprintf(&wptname[anzwp*64],"PAUSE %gh %gm (#%d)",(double)((int)((track[i].timestamp-track[j].timestamp)/6/60))/10.0,track[i].alt,j);
	} else {
	  sprintf(&wptname[anzwp*64],"PAUSE %g min %gm (#%d)",(double)((int)((track[i].timestamp-track[j].timestamp)/6))/10.0,track[i].alt,j);
        }
        liste[anzwp]=i;
        anzwp++;
	k=i;
      }
    } else { /* Ansonsten wende die Kritereien an, um herauszufinden, ob das 
                ein Wegpunkt werden soll */
      if((dt=track[i].timestamp-track[k].timestamp)>=itn_wayptsep2*60
     || (d=distance(track[i].lon,track[i].lat,track[k].lon,track[k].lat)*1000)>=itn_wayptdist2
     || (cd=fabs(track[i].kurs-track[k].kurs))>=itn_wayptdeg2
     || (dt>=itn_wayptsep*60 && d>=itn_wayptdist && cd>=itn_wayptdeg)) {    
 	sprintf(&wptname[anzwp*64],"v=%g alt=%g (#%d)",track[i].speed*KNOTEN,track[i].alt,j);

        liste[anzwp]=i;
        anzwp++;
        k=i;
      }
    }
  } /* for */
  
  /* Jetzt noch einen letzten Wegpunkt als Abschluss definieren. 
     k= letzter WP
     j=letzter gueltige Trackpunkt
     i kann sowohl gueltig al ungueltig sein. */

  /* Wenn letzter Trackpunkt ungueltig ist, nimm letzten gueltigen */
  if(track[i].speed==0 || track[i].quality==0 || track[i].timestamp<24*60*60.0) i=j;

  if(i!=k) {  /*Wenn schon vorhanden, keinen neuen definieren.*/
    sprintf(&wptname[anzwp*64],"END (#%d)",i);
    liste[anzwp]=i;
    anzwp++;
  }
  return(anzwp);
}


void save_itnfile(char *itnfilename) {
  FILE *dptr;
  double d,cd,dt;
  int h,m;
  int i=0;
  int j,k,flag;
  char line[256];
  int n=0;
  char *times;

  if(verbose>0) printf("--> %s [",itnfilename); 
  dptr=fopen(itnfilename,"w");
  if(!dptr) {
    printf("Failed to open itn file %s\n",itnfilename);
    return;
  }
  while((track[i].quality==0 || track[i].timestamp<24*60*60) && i<anztrack) i++;

  if(itn_localtime) {
    times=vs_time2(track[i].timestamp);
  } else {
    d=track[i].time;
    h=(int)(d/10000);d-=h*10000;      
    m=(int)(d/100);d-=m*100;
    times=malloc(13);
    sprintf(times,"%02d:%02d:%02d",h,m,(int)d);
  }
  
  fprintf(dptr,"%d|%d|%s Start (#%d)|0|\n",(int)(track[i].lon*100000.0),
		 (int)(track[i].lat*100000.0),times,i);
  free(times);
  n++;
  k=i;
  for(j=i;i<anztrack;j=i++) {
    flag=0;
    while((track[i].speed==0 || track[i].quality==0 || track[i].timestamp<24*60*60.0) && i<anztrack) {
      i++;
      if(verbose>2) printf(".");
      flag=1;
    }
    if(flag) {
      if(track[i].timestamp-track[j].timestamp>itn_halttime) {

	if(itn_localtime) {
	  times=vs_time2(track[j].timestamp);
	  times[6]=0;
	} else {
  	  d=track[j].time;
	  h=(int)(d/10000);d-=h*10000;      
          m=(int)(d/100);d-=m*100;
          times=malloc(9);
          sprintf(times,"%02d:%02d",h,m);
	}
        if(verbose>1) printf("%s %g-->%g %02d:%02d:%02d\n",ITN_PAUSE,
	  track[j].time,track[i].time,(int)h,(int)m,(int)d);
        if((track[i].timestamp-track[j].timestamp)>100*60) {
          sprintf(line,"%s %gh Pause (#%d)",times,(double)((int)((track[i].timestamp-track[j].timestamp)/6/60))/10.0,j);
	} else {	
          sprintf(line,"%s %g min Pause %gm (#%d)",times,
		(double)((int)((track[i].timestamp-track[j].timestamp)/6))/10.0,track[i].alt,j);
        }
        fprintf(dptr,"%d|%d|%s|0|\n",(int)(track[i].lon*100000),(int)(track[i].lat*100000),line);
	k=i;
	n++;
	free(times);
      }
    } else {
      if((dt=track[i].timestamp-track[k].timestamp)>=itn_wayptsep2*60
     || (d=distance(track[i].lon,track[i].lat,track[k].lon,track[k].lat)*1000)>=itn_wayptdist2
     || (cd=fabs(track[i].kurs-track[k].kurs))>=itn_wayptdeg2
     || (dt>=itn_wayptsep*60 && d>=itn_wayptdist && cd>=itn_wayptdeg)) {    
	if(itn_localtime) {
	  times=vs_time2(track[i].timestamp);
	  times[6]=0;
	} else {
  	  d=track[i].time;
	  h=(int)(d/10000);d-=h*10000;      
          m=(int)(d/100);d-=m*100;
          times=malloc(9);
          sprintf(times,"%02d:%02d",h,m);
	}
        if(verbose>1) printf("%02d:%02d:%02d v=%g %s\n",h,m,(int)d,track[i].speed*KNOTEN,ITN_ETAPPE);
        sprintf(line,"%s v=%g alt=%g (#%d)",times,track[i].speed*KNOTEN,track[i].alt,j);
        fprintf(dptr,"%d|%d|%s|0|\n",(int)(track[i].lon*100000.0),(int)(track[i].lat*100000.0),line);
        k=i;
	n++;
	free(times);
      }
    }
  }
  if(itn_localtime) {
    times=vs_time2(track[anztrack-1].timestamp);
  } else {
    d=track[anztrack-1].time;
    h=(int)(d/10000);d-=h*10000;      
    m=(int)(d/100);d-=m*100;
    times=malloc(13);
    sprintf(times,"%02d:%02d:%02d",h,m,(int)d);
  }
  fprintf(dptr,"%d|%d|%s End (#%d)|2|\n",(int)(track[anztrack-1].lon*100000.0),(int)(track[anztrack-1].lat*100000.0),
                                               times,anztrack);
  free(times);
  if(verbose>0) printf("] %d items.\n",n);
  fclose(dptr);
}
