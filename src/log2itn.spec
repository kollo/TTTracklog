#
# spec file for package log2itn (Version 1.00), by Markus Hoffmann
#
# Copyright  (c) Markus Hoffmann 2009
#
# please send bugfixes or comments to kollo@users.sourceforge.net.
#

Summary: log2itn -- a converter (filter) for NMEA log files.
Vendor: 
Name: log2itn
Version: 1.13
Release: 1
Copyright: GPL
Group: Productivity/Networking/Other
Source: http://www.opentom.org/Log2itn/log2itn-1.13.tar.gz
URL: http://www.opentom.org/Log2itn
Packager: Markus Hoffmann <kollo@users.sourceforge.net>
#BuildRequires:	cslib
# Requires:	tine

BuildRoot: /var/tmp/%{name}-buildroot

%description

log2itn is a converter (filter) for NMEA log files. It converts them into .gpx
and/or .itn files according to divers parameters. It also shows the summaries
about your saved tracks. 

Authors:
--------
   Markus Hoffmann <kollo@users.sourceforge.net>, 

%prep
%setup -q

%build
(make log2itn-l)
%install
rm -rf $RPM_BUILD_ROOT
#install -d $RPM_BUILD_ROOT%{_mandir}/man1
#install -d $RPM_BUILD_ROOT%{_mandir}/man5
#install -d $RPM_BUILD_ROOT/usr/bin
#install -d $RPM_BUILD_ROOT/etc/init.d
#install -m 775 -d $RPM_BUILD_ROOT/usr/share/cs
#install -m 664 extra-db.dat $RPM_BUILD_ROOT/usr/share/cs/


#install -m 755 cskernel.init.rc.sh $RPM_BUILD_ROOT/etc/init.d/cskernel
install -m 755 log2itn-l $RPM_BUILD_ROOT/usr/bin/log2itn
#install -m 644 cskernel.1 $RPM_BUILD_ROOT%{_mandir}/man1/cskernel.1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README COPYING INSTALL ACKNOWLEGEMENTS AUTHORS RELEASE_NOTES HISTORY
#%doc ddd/doc/DOOCS-ddd-manual.pdf
#%doc doc/DOOCS_MANUAL.pdf

/usr/bin/log2itn
#/etc/init.d/cskernel
#/usr/share/man/man1/cskernel.1.gz


%changelog
* Tue Nov 16 2007 Markus Hoffmann <markus.hoffmann@desy.de>
  1st release
