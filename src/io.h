/* io module files etc  */
/*(c) Markus Hoffmann 2008 */

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

char *lineinput( FILE *n, char *line);
int myeof(FILE *n);


int exist(char *filename);
size_t lof(FILE *n);

#ifndef TRUE
#define FALSE (0)
#define TRUE (1)
#endif
