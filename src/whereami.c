/* whereami    erfragt geo informationen from  TomTom  */
/*(c) Markus Hoffmann 2008-2009 */


/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
//#include <signal.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>


#include <string.h>
#include <time.h>
#include <math.h>
//#include <termios.h>
#include <sysexits.h>
#include <sys/time.h>
#include <fcntl.h>

#include "io.h"

#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a>b)?a:b)

#define whereamiVERSION "1.02"

void usage();

int verbose=0;

char outputfilename[256]="a.itn";
char inputfilename[256]="input.itn";
char format[256]="%L|%B|%y %c,%s %h-%H %X";

void intro() { 
  puts("whereami V." whereamiVERSION " (c) 2008-2009 Markus Hoffmann");
}
void usage() { 
  puts("\nUsage: "
       " whereami [options] [filename.itn] --- get geo information\n\n"
       "-h --help           --- Usage\n"
       "--lon <longitude>   --- use longitude\n"
       "--lat <lattitude>   --- use lattitude\n"
       "-f <format string>  --- specify the output format [%L|%l|%T %N|0]\n"
       "-o <filename>       --- put output to file\n"
       "-v                  --- be more verbose\n"
       "-q                  --- be more quiet");
}

int wort_sep (char *t,char c,int klamb ,char *w1, char *w2)    {
  int f=0, klam=0;
  if(!(*t)) return(*w1=*w2=0);
  while(*t && (*t!=c || f || klam>0)) {
    if(*t=='"') f=!f;
    else if(!f && (((klamb&1) && *t=='(') || ((klamb&2) && *t=='[') || ((klamb&4) && *t=='{'))) klam++;
    else if(!f && (((klamb&1) && *t==')') || ((klamb&2) && *t==']') || ((klamb&4) && *t=='}'))) klam--;
    *w1++=*t++;
  }
  if(!(*t)) {*w2=*w1=0;return(1);} else {*w1=0;strcpy(w2,++t);return(2);}
}
char *vs_time() {
  char *ergebnis;
  time_t timec;
  struct tm * loctim;
  timec = time(&timec);
  ergebnis=malloc(9);
  loctim=localtime(&timec);
  strncpy(ergebnis,ctime(&timec)+11,8);
  ergebnis[8]=0;
  return(ergebnis);
}
char *vs_date() {
  char *ergebnis;
  time_t timec;
  struct tm * loctim;
  ergebnis=malloc(12);
  timec = time(&timec);
  loctim=localtime(&timec);
  sprintf(ergebnis,"%04d-%02d-%02d",1900+loctim->tm_year,loctim->tm_mon+1,loctim->tm_mday);
  return(ergebnis);
}

#define MESSAGE  "/var/run/SDK.TomTomNavigationServer.%d.%d.message"
#define FINISH   "/var/run/SDK.TomTomNavigationServer.%d.%d.finished"
#define AFINISH  "/var/run/TomTomNavigationServer.SDK.%d.%d.finished"
#define AMESSAGE "/var/run/TomTomNavigationServer.SDK.%d.%d.message"


int SDK_Interaction(char *message, char *result) {
  static int icounter=0;
  char filename1[100];
  char filename2[100];
  char filename3[100];
  char filename4[100];
  char footer[2];
  int i,e;
  int pid=getpid();
  footer[0]=0;
  FILE *dptr;

#ifdef FAKE 
  char w1[100],w2[100]; 
  printf("--> %s\n",message);
  sprintf(result,"0|1|999900|5333300|61|200");
  e=wort_sep(result,'|',0,w1,w2);
  return((int)atof(w1));
#else
  sprintf(filename1,MESSAGE,pid,icounter);
  sprintf(filename2,FINISH,pid,icounter);
  sprintf(filename3,AFINISH,pid,icounter);
  sprintf(filename4,AMESSAGE,pid,icounter);
  icounter++;

  dptr=fopen(filename1,"w");
  fwrite(message,strlen(message),1,dptr);
  fwrite(footer,1,1,dptr);
  fclose(dptr);

  dptr=fopen(filename2,"w");
  fprintf(dptr,"finish\n");
  fclose(dptr);

  i=0;
  while(!exist(filename3) && i<10) {sleep(1);i++;}
  if(!exist(filename4) || i==10) {
    fprintf(stderr,"ERROR: timeout waiting for answer.\n");
    return(-1);
  } else {
    char w1[100],w2[100];
  
    dptr=fopen(filename4,"r");
    fread(result,lof(dptr),1,dptr);
    fclose(dptr);
    e=wort_sep(result,'|',0,w1,w2);
    return((int)atof(w1));
  }
#endif  
}
#if 0
int SDK_FlashMessage(char *n, int duration) {
  char message[100];
  char dummy[100];
  sprintf(message,"FlashMessageV01|%s|%d|",n,duration);
  return(SDK_Interaction(message,dummy));
}
#endif
int SDK_GetPosition(double *lon,double *lat,double *speed, double *dir) {
  char message[100];
  char result[100];
  int e,ee;
  int valid;
  sprintf(message,"GetCurrentPositionV01|");
  e=SDK_Interaction(message,result);
  if(e==0) {
    char w1[100],w2[100];
    ee=wort_sep(result,'|',0,w1,w2);
    ee=wort_sep(w2,'|',0,w1,w2);
    valid=atoi(w1);
    ee=wort_sep(w2,'|',0,w1,w2);
    *lon=(double)atoi(w1)/100000;
    ee=wort_sep(w2,'|',0,w1,w2);
    *lat=(double)atoi(w1)/100000;
    ee=wort_sep(w2,'|',0,w1,w2);
    *speed=atof(w1);
    ee=wort_sep(w2,'|',0,w1,w2);
    *dir=atof(w1);
    if(!valid) *speed=-1;
  }
  return(e);
}
#define LOC_TYP_POS 0
#define LOC_TYP_NODE 1
#define LOC_TYP_CROSS 2
#define LOC_TYP_ADDR 3
#define LOC_TYP_STREET 4


int SDK_GetLocationInfo(double lon,double lat, double *rlon, double *rlat, 
                    int *typ, char *name, char *street, char *city, 
		    int *number1, int *number2) {
  char message[100];
  char result[100];
  
  int e;
  sprintf(message,"GetLocationInfoEx|%d|%d|",(int)(lon*100000),(int)(lat*100000));
  e=SDK_Interaction(message,result);
  if(e==0) {
    char w1[100],w2[100];
    int ee;
    ee=wort_sep(result,'|',0,w1,w2);    
    ee=wort_sep(w2,'|',0,w1,w2);
    *rlon=(double)atoi(w1)/100000;
    ee=wort_sep(w2,'|',0,w1,w2);
    *rlat=(double)atoi(w1)/100000;
    ee=wort_sep(w2,'|',0,w1,w2);
    *typ=atoi(w1);
    strcpy(name,w2);
    ee=wort_sep(w2,'|',0,w1,w2);
    strcpy(street,w1);
    ee=wort_sep(w2,'|',0,w1,w2);
    *number1=atoi(w1);
    ee=wort_sep(w2,'|',0,w1,w2);
    *number2=atoi(w1);
    ee=wort_sep(w2,'|',0,w1,w2);
    strcpy(city,w1);
  }
  return(e);
}
double speed=0,dir=0,altitude=-4711;

void do_output(double lon, double lat, char *data) {
  double nlon=999999,nlat=999999;
  int typ,hn1,hn2;
  char name[128];
  char street[128];
  char city[128];

  if(!SDK_GetLocationInfo(lon,lat,&nlon,&nlat,&typ,name,street,city,&hn1,&hn2)) {
      int i=0,j=0;
      char buf[512]="";
      char a;
      if(verbose) printf("lon=%g lat=%g typ=%d name='%s'\n",nlon,nlat,typ,name);
      while((a=format[i])) {
        if(a=='%') {
          a=format[++i];
	  if(a=='%') buf[j++]=a;
	  else if(a=='L') {
	    sprintf(&buf[j],"%d",(int)(lon*100000));
	    j=strlen(buf);
	  } else if(a=='B') {
	    sprintf(&buf[j],"%d",(int)(lat*100000));
	    j=strlen(buf);
	  } else if(a=='o') {
	    sprintf(&buf[j],"%g",nlon);
	    j=strlen(buf);
	  } else if(a=='l') {
	    sprintf(&buf[j],"%g",lon);
	    j=strlen(buf);
	  } else if(a=='p') {
	    sprintf(&buf[j],"%g",nlat);
	    j=strlen(buf);
	  } else if(a=='b') {
	    sprintf(&buf[j],"%g",lat);
	    j=strlen(buf);
	  } else if(a=='a') {
	    sprintf(&buf[j],"%g",altitude);
	    j=strlen(buf);
	  } else if(a=='v') {
	    sprintf(&buf[j],"%g",speed);
	    j=strlen(buf);
	  } else if(a=='d') {
	    sprintf(&buf[j],"%g",dir);
	    j=strlen(buf);
	  } else if(a=='T') {
	    char *times;
	    times=vs_time();
	    sprintf(&buf[j],"%s",times);
	    free(times);
	    j=strlen(buf);
	  } else if(a=='D') {
	    char *times;
	    times=vs_date();
	    sprintf(&buf[j],"%s",times);
	    free(times);
	    j=strlen(buf);
	  } else if(a=='y') {
	    sprintf(&buf[j],"%d",typ);
	    j=strlen(buf);
	  } else if(a=='n') {
	    sprintf(&buf[j],"%s",name);
	    j=strlen(buf);
	  } else if(a=='X') {
	    sprintf(&buf[j],"%s",data);
	    j=strlen(buf);
	  } else if(a=='c') {
	    sprintf(&buf[j],"%s",city);
	    j=strlen(buf);
	  } else if(a=='s') {
	    sprintf(&buf[j],"%s",street);
	    j=strlen(buf);
	  } else if(a=='h') {
	    sprintf(&buf[j],"%d",hn1);
	    j=strlen(buf);
	  } else if(a=='H') {
	    sprintf(&buf[j],"%d",hn2);
	    j=strlen(buf);
	  }
	} else buf[j++]=format[i];
        i++;
      }
      puts(buf);
    } 
}

void processline(char *n) {
  char w1[strlen(n)],w2[strlen(n)];
  int e;
  double lat,lon;
  e=wort_sep(n,'|',0,w1,w2);
  lon=(double)atoi(w1)/100000;
  e=wort_sep(w2,'|',0,w1,w2);
  lat=(double)atoi(w1)/100000;
 // printf("lon=%g, lat=%g, data=%s\n",lon,lat,w2);
  do_output(lon,lat,w2);
}

void process_itnfile(char *itnfilename) {
  FILE *dptr;
  char line[256];
  int count=0;

  if(verbose>0) printf("<-- %s [",itnfilename);
  dptr=fopen(itnfilename,"r");
  while(!myeof(dptr)) {
    lineinput(dptr,line);
    processline(line);
  }
  if(verbose>0) printf("] %d elements.\n",count);
  fclose(dptr);
}

int main(int argc, char** argv) {
  int DoExit=0;
  double lon=999999,lat=999999;

  if(argc>1) {
    int count;
    for(count=1;count<argc;count++) {    
      if(strcmp(argv[count],"-h")==0 || strcmp(argv[count],"--help")==0) {
        usage();
        DoExit=1;
      } else if(strcmp(argv[count],"-v")==0) {
        verbose++;
      } else if(strcmp(argv[count],"-q")==0) {
        verbose--;
      } else if(strcmp(argv[count],"--lon")==0) {
        count++;
	lon=atof(argv[count]);
      } else if(strcmp(argv[count],"--lat")==0) {
        count++;
	lat=atof(argv[count]);
      } else if(strcmp(argv[count],"-f")==0) {
        count++;
  	strcpy(format,argv[count]);
      } else if(strcmp(argv[count],"-o")==0) {
        count++;
  	strcpy(outputfilename,argv[count]);
      } else {
        strcpy(inputfilename,argv[count]);
      }
    }
  }
  if(DoExit) return(EX_OK);

  if(exist(inputfilename)) {
     process_itnfile(inputfilename);
     exit(EX_OK);
  } 

  if(lon==999999 || lat==999999) {
    if(!SDK_GetPosition(&lon,&lat,&speed,&dir)) {
      if(verbose)  printf("lon=%g lat=%g speed=%g dir=%g\n",lon,lat,speed,dir);
    } else return(EX_UNAVAILABLE);
  }
  if(lon!=999999 && lat!=999999) {
    do_output(lon,lat,"|0|");
  }
//  SDK_FlashMessage("whereami finished.",8000);
  return(EX_OK);
}

