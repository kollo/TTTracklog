/* nmea module konvertiert nmea-log files in brauchbare itn Routen-Files fuer TomTom  */
/*(c) Markus Hoffmann 2008 */

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */



void nmea_processline(char *n);
void load_logfile(char *logfilename);
int wort_sep (char *t,char c,int klamb ,char *w1, char *w2);
time_t timestamp(int D,int M,int J,int h, int m,int s);
char *vs_time();
char *vs_date();
char *vs_time2(time_t timec);
char *vs_date2(time_t timec);
