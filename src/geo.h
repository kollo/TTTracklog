/* This file is part of TTTracklog, the TomTom GPS Track logger (c) 2008-2009 by Markus Hoffmann
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */
 
#ifndef KNOTEN 
#define KNOTEN 1.852
#endif
#define DEG2RAD (3.14159265358979323846/180.0)


double distance(double lona,double lata,double lonb,double latb);
char *qthlocator(double breite,double laenge);
char *breite(double x);
char *laenge(double x);
char *h2t(double x);

double wgs84_to_msl_delta(double lat, double lon);
