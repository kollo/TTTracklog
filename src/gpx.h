/* gpx.h                            (c) Markus Hoffmann  2007-2008
*/
/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

extern double gpx_trackptsep;
extern double gpx_trackptsep2;
extern double gpx_trackptdist;
extern double gpx_trackptdist2;
extern double gpx_trackptdeg;
extern double gpx_trackptdeg2;
extern int gpx_name_flags;
extern int gpx_halttime;
extern int dodecimate;
extern int do_gpxwp;
extern double distance_significance;

#define GPX_SAT       1
#define GPX_MODE1     2
#define GPX_MODE2     4
#define GPX_MODE      6
#define GPX_FIX       8
#define GPX_DOP    0x10
#define GPX_MAGVAR 0x20
#define GPX_DGPS   0x40


void gpx_init();
void gpx_close();
void gpx_tracksep(FILE *dptr);
int save_gpxtrack(FILE *dptr,int number, char *name,int starttrack,int anztrack);
void save_gpxfile(char *gpxfilename);
char *gpx_time(double t,double d);
void get_stats();
int get_tours();
void get_bounds(int i,int anztrack,TOUR *stat);
void print_statistik(FILE *sysptr);
int save_gpxwaypoints(FILE *,int *liste,int anzlist);
