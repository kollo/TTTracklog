/* itn module konvertiert nmea-log files in brauchbare itn Routen-Files fuer TomTom  */
/*(c) Markus Hoffmann 2008-2010 */

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#define MAX_ANZWP 4000      /* Maximale Anzahl von Wegpunkten*/

#define DEFAULT_WAYPTSEP  20    /* minimal time between WPT in minutes */
#define DEFAULT_WAYPTSEP2 60*24 /* maximal time between WPT in minutes */ 
#define DEFAULT_WAYPTDIST  100      /* in meters */
#define DEFAULT_WAYPTDIST2 100*1000 /* in meters */
#define DEFAULT_HALTTIME  29    /* in seconds */


#define ITN_ETAPPE "ETAPPE"
#define ITN_PAUSE  "PAUSE"

/* function prototypes */

void save_itnfile(char *itnfilename);
int get_waypoints(int starttrack,int anztrack,int *liste);
