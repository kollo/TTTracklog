/* alt module konvertiert nmea-log files in altitude files fuer TomTom  */
/*(c) Markus Hoffmann 2008 */

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */


#include <stdlib.h>
#include <stdio.h>

#include "TTTracklog.h"
#include "geo.h"

extern TRACKPOINT *track;
extern int anztrack;
extern int verbose;

void save_altfile(char *altfilename) {
  FILE *dptr;
  double d;
  int i=0,last=0;
  int k;
  int n=0;
  double meanalt=0,ldist=0,gdist=0;
  dptr=fopen(altfilename,"w");
  if(!dptr) {
    printf("Failed to open altfile %s.\n",altfilename);
    return;
  }
  if(verbose>0) printf("--> %s [",altfilename);
  fprintf(dptr,"# Altidude statistcs, created by TTTracklog V." TTTracklogVERSION " " COPYRIGHT "\n");
  fprintf(dptr,"# anztrack=%d\n",anztrack);
  fprintf(dptr,"# timestamp[s] dist[m] alt[m] speed[km/h]\n");

  for(;i<anztrack;i++) {
    while(track[i].quality==0 && i<anztrack) i++;
    k=i;
    meanalt=0;
    while((track[i].speed==0 && track[i].quality!=0) && i<anztrack) {
      meanalt+=track[i].alt;
      i++;
    }
    if(i==k) meanalt=track[i].alt;
    else meanalt=meanalt/(i-k);
    if(n) {
      d=distance(track[last].lon,track[last].lat,track[i].lon,track[i].lat)*1000;
      if(d<10000) ldist+=d;
    }
  //  printf("%g\n",track[i].timestamp-track[last].timestamp);
    if(ldist>100 || (track[i].timestamp-track[last].timestamp>=60.0)
    || (track[i].alt-track[last].alt>=1)
    || n==0) {
      gdist+=ldist;
      fprintf(dptr,"%d %g %g %g\n",(int)track[i].timestamp,gdist,meanalt,track[i].speed*KNOTEN);
      last=i;
      ldist=0;
      n++;
    }
  }
  if(verbose>0) printf("] %d items.\n",n);
  fclose(dptr);
}
