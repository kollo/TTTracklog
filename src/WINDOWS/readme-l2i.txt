
log2itn(1)          a converter (filter) for NMEA log files         log2itn(1)

NAME
       log2itn  - converter (filter) for NMEA log files

SYNOPSIS
       log2itn [ options ] <filename>

DESCRIPTION
       log2itn  is  a  converter (filter) for NMEA log files. It converts them
       into .gpx and/or .itn files according to  divers  parameters.  It  also
       shows the summaries about your saved tracks.

       log2itn  permits  to generate new .itn and .gpx files from the original
       .log file.

       Example of use:

       I make a nice tour. Back at home, I  realize  that  I  have  set  wrong
       parameters  in the TTTracklog-wrapper commandline and i?m not satisfied
       with my .itn and .gpx files. Using log2itn gives me the possibility  to
       generate new files like I want.

       For  a  desciption  of  TTracklog  and the usage on a TomTom Navigation
       device see http://www.opentom.org/TTTracklog 

USAGE ON WINDOWS OPERATING SYSTEMS:
===================================

English
=======

To use log2itn.exe and log2itn.bat, proceed as follow:

1. Make sure that log2itn.exe, log2itn.bat and the .log file you want to
   convert are in the same directory
2. Modify(*) log2itn.bat and set the parameters you want
3. Save and close log2itn.bat
4. Run log2itn.bat by a double mouse click

* To modify, do a right mouse click on the file and select "modify"


Deutsch
=======

Damit Du das log2itn.exe und log2itn.bat brauchen kannst, gehe wie folgt vor:

1. Pr�fe, dass log2itn.exe, log2itn.bat und die .log Datei, die Du konvertieren
   m�chtest, sich alle im gleichen Verzeichnis befinden, 
2. �ndere(*) log2itn.bat und setze die gew�nschten Parameter
3. Speichere und schliesse das log2itn.bat
4. Starte das log2itn.bat mittels Doppelklick

* Klick mit der rechte Maustaste auf der Datei und "�ndern" w�hlen


Fran�ais
========

Pour utiliser log2itn.exe et log2itn.bat, proc�de comme suit:

1. Assure-toi que log2itn.exe, log2itn.bat et le fichier .log que tu d�sires
   convertir se trouvent tous dans le m�me r�pertoire
2. Modifie(*) log2itn.bat et saisis les param�tres d�sir�s
3. Enregistre et ferme log2itn.bat
4. D�marre log2itn.bat par un double clic

* Pour modifier, fais un clic sur le fichier avec la touche de droite de la
souris et s�lectionne "modifier"

===============================================================================

Filenames
       log2itn processes input files with any extention. Anyway, it should  be
       NMEA-log files.

OPTIONS
       By  default,  and if no options are given, log2itn calculates the track
       summary statistics and prints it to the stdout.

       -a file.alt
              Place altitude output to file file.alt.

       --aac  altitude auto correction on. This only works with nmea  logfiles
              where  the geoid height field is valid. (This will not work e.g.
              for log files produced by TomTom GPS chipsets.)

       --agc  do altitude correction using a simple geoid model. This  can  be
              used,  if the altitude values in the input logfile are altitudes
              over WG84 and not above sea level and you want  it  to  be  con?
              verted into height above sea level.

       --amc <meters>
              do a manual altitude correction by subtracting <meters> from the
              altitudes.  Default: <0>

       --decimate
              decimate the generated points (in gpx) with  a  Douglas-Peucker-
              Algorithmus.  This  option  will produce smaller .gpx files with
              less points, but with only little loss of information. The  sim?
              plified  curve  consists  of a subset of the points that defined
              the original track.

       -g file.gpx
              Place gpx output to file file.gpx.

       --gc <degrees>[,<degrees>]
              set minimal [and maximal] gpx direction difference in degrees.

       --gh <seconds>
              set gpx tracksep halt time in seconds. Default: <2>

       --gi <seconds>[,<seconds>]
              set minimal [and maximal] gpx trackpoint time interval  in  sec?
              onds. Default: <1>

       --gm <meters>[,<meters>]       
             set minimal [and maximal] gpx trackpoint distance in meters.

       --gnd set tour distance as trackpoint name in the gpx file.

       --gnn  set trackpoint number as trackpoint name in the gpx file.

       --gnt  set local time as trackpoint name in the gpx file.

       --gwp  also save waypoints into gpx file.

       -h, --help
              print a short help message and exit.

       --ic <degrees>[,<degrees>]
              set minimal [and maximal] itn direction difference in degrees.

       --ih <seconds>
              set  itn  (waypoint)  pause time in seconds. Default: <29>. More
              than this nomber of seconds in rest will generate a waypoint.

       --ii <minutes>[,<minutes>]
              set minimal [and maximal] itn waypoint time interval in minutes.
              Default: <20>

       --il   don?t  set  local  time  (instead  use  GMT) for data in the itn
              files.

       --im <meters>[,<meters>]
              set minimal [and maximal] itn waypoint distance in meters.

       -o file.itn
              Place itn output to file file.itn.

       -q     be more quiet.

       --ti <seconds>
              set tour separation time in seconds. Default: <16>

       -v     be more verbose.

       --version
              shows version, program and other information and exits.

EXAMPLES
       log2itn mylogfile.log
           prints statistics like track lengths etc about the nmea-log file.

       log2itn --gwp -g newfile.gpx mylogfile.log
           converts the nmea-log input (mylogfile.log) to gpx tracks. Also include
           waypoints (pause times etc.) into the file.

VERSION
       Page was created for V.1.14

BUG REPORTS
       If  you  find a bug in the log2itn converter, you should report it. But
       first,  you  should make sure that it really is  a  bug,  and  that  it
       appears  in  the  latest version of the log2itn/TTTracklog package that
       you have.


       Once  you have determined that a bug actually exists, mail a bug report
       to  kollo@users.sourceforge.net.  If  you have a fix, you  are  welcome
       to  mail that as well!  Suggestions  may be  mailed  to the  discussion
       page  on  http://www.opentom.org/Log2itn  or posted to the bug tracking
       system.

       Comments and  bug  reports  concerning  this  manual   page  should  be
       directed to kollo@users.sourceforge.net.


AUTHOR
       Markus Hoffmann <kollo@users.sourceforge.net>

COPYRIGHT
       Copyright (C) 2007-2010 Markus Hoffmann <kollo@users.sourceforge.net>

       This  program is free software;  you  can  redistribute  it and/or mod?
       ify it under the terms of the GNU General Public License  as  published
       by  the  Free  Software  Foundation; either  version  2 of the License,
       or (at your option) any later version.

       This program is distributed in the hope that  it  will  be useful,  but
       WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of MER?
       CHANTABILITY or FITNESS  FOR  A  PARTICULAR PURPOSE.    See   the   GNU
       General  Public License for more details.

SEE ALSO
       TTTracklog(1), whereami(1)

Version 1.14                      19-Sep-2009                       log2itn(1)

