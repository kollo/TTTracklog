@ECHO OFF

rem *** Windows batch file for log2itn by Bernard 2009 ***

ECHO.
ECHO.
ECHO log2itn is processing, please wait...
ECHO.
ECHO.

rem *** Make sure that log2itn.exe, log2itn.bat and the .log file you want to convert are in the same directory. ***

log2itn -o log2itn.itn --ii 0 --im 5000 -g log2itn.gpx --gm 91 --gnt --gnd --ih 600 nmea-2009-05-23.log

rem *** If you want to drop a .log file on this .bat file, please use: ***
rem log2itn -o $1.itn --ii 0 --im 5000 -g $1.gpx --gm 91 --gnt --gnd --ih 600 $1



ECHO.
ECHO.
ECHO Processing terminated.
ECHO Please check if the generated files are correct.
ECHO.
ECHO.

PAUSE
