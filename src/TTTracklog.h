/* TTTracklog.h                            (c) Markus Hoffmann  2007-2008
*/
/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */



#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a>b)?a:b)
#define FALSE (0)
#define TRUE (1)

#ifndef KNOTEN 
#define KNOTEN 1.852
#endif

#define DATAPATH "/mnt/sdcard/statdata"

#define TTTracklogVERSION "1.14"
#define COPYRIGHT "(c) 2008-2010 Markus Hoffmann"

#define LOGFILE "%s/nmea-%s.log"
#define GPXFILE "%s/%s.gpx"
#define ITNFILE "/mnt/sdcard/itn/%s.itn"
#define SYSFILE "/mnt/sdcard/text/%s-TTTracklog.txt"
#define ALTFILE "/tmp/altitudes.dat"

void usage();
char *vs_date();

/* 86400 seconds per day plus 1000 for the write-buffer + some margin */

#define MAX_TRACK 90000

typedef struct {
  double lat;    /* Lattitude [deg] */
  double lon;    /* Longitude [deg] */
  double alt;    /* Altitude (sea level) [m] */
  double speed;  /* Speed [knots] */
  double kurs;   /* Course [deg]  */
  double time;
  double date;
  double quality;
  double timestamp;
  double magdev;
  double hdop,vdop,pdop;
  double dgpsalter;
  double dilution; /* horizontal dilution of precision, 0.5 -- 99.9*/
  double geosep;   /* Geoid separation alt in meters*/
  unsigned int flags;
  unsigned short dgpsid; /* ID of DGPS Reference Station */
  unsigned short numsat;
  short fix;
  char mode,mode2;
  unsigned char status;
} TRACKPOINT;

#define FLAG_PGLOR 1

#define MAX_TOUR 16

typedef struct {
  int start,stop;
  double maxspeed,minalt,maxalt,totdist,standzeit,wegzeit;
  double minlon,maxlon;
  double minlat,maxlat;
  double mintim,maxtim;
  int validtrack,anztrack;
} TOUR;


