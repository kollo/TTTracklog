
/* TTTracklog.c                            (c) Markus Hoffmann  2007-2010
*/

/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sysexits.h>

#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>

#include <termios.h>
#include <sys/ioctl.h>

#include <string.h>
#include <time.h>
#include <math.h>

#include <fcntl.h>

#include "TTTracklog.h"
#include "nmea.h"
#include "gpx.h"
#include "itn.h"
#include "io.h"

#define GPSFEED "/var/run/gpsfeed"
#define GPSPIPE "/var/run/gpspipe"

#define BUFSIZE (4096*6)

int verbose=0;
int dosyslog=1;
int dogpx=1;
int doitn=1;
int doclean=0;

#define USE_RAM (dosyslog || dogpx || doitn)

extern int altitude_correction_typ;
extern double altitude_correction;


char *datapath=DATAPATH;

char *gpsdevice=GPSFEED;

char *gpxfilename;
char *itnfilename;
char *logfilename;
char *sysfilename;

TRACKPOINT *track;
int anztrack=0;

char nmeabuffer[BUFSIZE];
int bufpointer=0;
char *date;

FILE *feedptr,*sysdptr;


extern double itn_wayptsep;
extern double itn_wayptsep2;
extern double itn_wayptdist;
extern double itn_wayptdist2;
extern double itn_wayptdeg;
extern double itn_wayptdeg2;
extern double itn_halttime;
extern int itn_localtime;

extern double nmea_toursep;

void usage() {
  puts("\nUsage: "
       " TTTracklog [options] --- start the track/nmea logger\n\n"
       "-h --help  --- Usage\n"
       "-d <path>  --- set folder for data files [" DATAPATH "]\n"
       "--gpsdev <file>  --- set NMEA input device [" GPSFEED "]\n"
       "-g         --- [don't] produce gpx files\n"
       "-i         --- [don't] produce itn files\n"
       "-a         --- [do] produce alt files\n"
       "-s         --- [don't] produce syslog file in text/\n"
       "--aac          --- use altitude auto correction\n"
       "--amc <value>  --- use manual altitude correction, value in meters [0]\n"
       "--agc          --- use altitude correction by simple geoid model\n"
       "--gi <seconds>[,<seconds>] --- set gpx trackpoint interval         [0] seconds\n"
       "--gm <meters>[,<meters>]   --- set minimum gpx trackpoint distance [0] meters\n"
       "--gc <degrees>[,<degrees>] --- set minimal gpx course difference   [0] degrees\n"
       "--gh <seconds> --- set gpx tracksep halt time  [2] seconds\n"
       "--gnt          --- set local time as trackpoint name\n"
       "--gnd          --- set tour distance as trackpoint name\n"
       "--gnn          --- set point number as trackpoint name\n"
       "--gwp          --- also save waypoints into gpx file\n"
       "--ii <minutes>[,<minutes>] --- set minimum (and maximum) itn waypoint time interval [20] minutes\n"
       "--im <meters>[,<meters>]   --- set minimum (and maximum) itn waypoint distance [100] meters\n"
       "--ic <degrees>[,<degrees>] --- set minimal (and maximal) itn course difference   [0] degrees\n"
       "--ih <seconds> --- set itn pause time                 [29] seconds\n"
       "--il           --- [don't] use local time (instead of GMT) for data in itn files\n"
       "--ti <seconds> --- set tour separation time           [16] seconds\n"
       "--clean    --- Empty GPS input buffer at startup\n"
       "--decimate --- Decimate gpx trackpoints\n"
       "-v         --- be more verbose\n"
       "-q         --- be more quiet");
}

void io_error(int n, char *s) {
  if(dosyslog) fprintf(sysdptr,"\nIO-ERROR:%d [%s] Abort!\n",n,s);
  printf("errno=%d\n",n);
  printf("IO-Error: %s\n",s);  /* Allgemeiner IO-Fehler */
  exit(EX_IOERR);
}


void save_log(int n) {
  FILE *dptr;
  if(verbose) printf("-->(%s)",logfilename);
  if(!exist(logfilename)) {
    dptr=fopen(logfilename,"a");
    if(!dptr) io_error(errno,"OPEN logfile");
    else {
      char *times=vs_time();
      fprintf(dptr,"# NMEA log created with TTTracklog V." TTTracklogVERSION
                                                  " (c) 2008-2010 Markus Hoffmann\n"
                   "# date=%s time=%s timestamp=%d\n",date,times,(int)time(NULL));
      free(times);
    }
  } else dptr=fopen(logfilename,"a");
  if(!dptr) io_error(errno,"OPEN logfile");
  else {
    fwrite(nmeabuffer,1,n,dptr);
    fclose(dptr);
  }
  if(verbose) printf("[%d]\n",n);
}

/* Aufgelaufenen Bufferinhalt rausschreiben und prozessieren */

void process_log() {
  int i,j,k;

  if(verbose>1) printf("Processing log-buffer: (%d Bytes) \n",bufpointer);
  if(bufpointer) { /* Sonst ist nichts zu tun */
    /* Ende der letzten vollstaendigen Zeile finden */
    for(i=bufpointer-1;(i>0 && nmeabuffer[i]!='\n');i--) ;

    /* Logfile rausschreiben (create or append)*/

    if(i==0) save_log(bufpointer); /* Hier waere alles ohne Zeilenende ???*/
    else save_log(i+1);
    
    if(verbose && dosyslog) fprintf(sysdptr," processing %d\n",anztrack); 
    if(USE_RAM) {
      /* Zeilenweise verarbeiten (von vorne) */
      for(j=0,k=0;j+k<bufpointer;) {
        for(j=0;j+k<bufpointer && nmeabuffer[k+j]!='\n';j++)  ;    
        if(nmeabuffer[k+j]=='\n') {
          nmeabuffer[k+j]=0;
          nmea_processline(&nmeabuffer[k]);
	  k+=j+1;
        }
      }   
    }
    /* buffer leeren, letzte unvollstaendige Zeile behalten */
    if(i==0) {
      if(verbose>=0) puts("WARNING: Buffer discarded.");
      bufpointer=0; /* Hier stimmt was nicht, alles wegwerfen */
    } else if(i==bufpointer-1) bufpointer=0; /* Alles OK, nichts zu uebertragen*/
    else {
      i++;
      if(verbose) printf("%d bytes fragmentary: <%s>\n",bufpointer-i,&nmeabuffer[i]);
      for(j=0;j<bufpointer-i;j++) nmeabuffer[j]=nmeabuffer[i+j];
      bufpointer-=i;
    }
  }
}

void break_handler( int signum) {
  if(dosyslog) fprintf(sysdptr,"]\n ** BREAK signal received. Terminating...\n");
  process_log(); 
  if(USE_RAM) get_stats();
  if(dogpx && anztrack) save_gpxfile(gpxfilename);
  if(doitn && anztrack) save_itnfile(itnfilename);
  if(dosyslog) {
    print_statistik(sysdptr);
    fclose(sysdptr);
  }
  signal(SIGINT, SIG_DFL);
  raise(signum);
}

extern void save_altfile(char *altfilename);

void signal_handler( int signum) {
  if(dosyslog) fprintf(sysdptr,"]\n ** SIGNAL %d received.\n",signum);
  process_log(); 
  if(USE_RAM) get_stats();
  if(dogpx && anztrack) save_gpxfile(gpxfilename);
  if(doitn && anztrack) save_itnfile(itnfilename);
  if(signum==SIGHUP) {
    if(dosyslog) fprintf(sysdptr,"Spill out...\n");
    if(anztrack && USE_RAM) save_altfile(ALTFILE); 
    if(dosyslog) fflush(sysdptr);
    signal(signum,signal_handler);
  } else {
    if(dosyslog) {
      print_statistik(sysdptr);
      fprintf(sysdptr,"###\n### Terminating...\n");
      fclose(sysdptr);
    }
    signal(signum,SIG_DFL);
    raise(signum);
  }
}



int main(int argc, char** argv) {
  int DoExit=0;
  int t,e;
  char w1[100],w2[100];

  if(argc>1) {
    int count;
    for(count=1;count<argc;count++) {
      if(strcmp(argv[count],"-h")==0 || strcmp(argv[count],"--help")==0) {
        usage();
        DoExit=1;
      }
      else if(strcmp(argv[count],"-v")==0) verbose++;
      else if(strcmp(argv[count],"-q")==0) verbose--;
      else if(strcmp(argv[count],"-s")==0) dosyslog=!dosyslog;
      else if(strcmp(argv[count],"-g")==0) dogpx=!dogpx;
      else if(strcmp(argv[count],"-i")==0) doitn=!doitn;
      else if(strcmp(argv[count],"-d")==0)       datapath=argv[++count];
      else if(strcmp(argv[count],"--gpsdev")==0) gpsdevice=argv[++count];
      else if(strcmp(argv[count],"--gi")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	gpx_trackptsep=atof(w1);
	if(e==2) gpx_trackptsep2=atof(w2);
      } else if(strcmp(argv[count],"--gm")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	gpx_trackptdist=atof(w1);
	if(e==2) gpx_trackptdist2=atof(w2);
      } else if(strcmp(argv[count],"--gc")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	gpx_trackptdeg=atof(w1);
        if(e==2) gpx_trackptdeg=atof(w2);
      } else if(strcmp(argv[count],"--aac")==0) altitude_correction_typ^=1;
      else if(strcmp(argv[count],"--agc")==0)   altitude_correction_typ^=2;
      else if(strcmp(argv[count],"--amc")==0) {
	altitude_correction_typ^=4;
	count++;
	altitude_correction=atof(argv[count]);
      } else if(strcmp(argv[count],"--gnt")==0) gpx_name_flags^=1;
      else if(strcmp(argv[count],"--gnd")==0)   gpx_name_flags^=2;
      else if(strcmp(argv[count],"--gnn")==0)   gpx_name_flags^=4;
      else if(strcmp(argv[count],"--gwp")==0)   do_gpxwp=!do_gpxwp;
      else if(strcmp(argv[count],"--ii")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	itn_wayptsep=atof(w1);
	if(e==2) itn_wayptsep2=atof(w2);
      } else if(strcmp(argv[count],"--im")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	itn_wayptdist=atof(w1);
	if(e==2) itn_wayptdist2=atof(w2);
      } else if(strcmp(argv[count],"--ic")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	itn_wayptdeg=atof(w1);
	if(e==2) itn_wayptdeg2=atof(w2);
      } else if(strcmp(argv[count],"--ih")==0) {
        count++;
	itn_halttime=atof(argv[count]);
      } else if(strcmp(argv[count],"--gh")==0) {
        count++;
	gpx_halttime=atof(argv[count]);
      } else if(strcmp(argv[count],"--il")==0) {
        itn_localtime=!itn_localtime;
      } else if(strcmp(argv[count],"--ti")==0) {
        count++;
	nmea_toursep=atof(argv[count]);
      } else if(strcmp(argv[count],"--clean")==0)  doclean=!doclean;
      else if(strcmp(argv[count],"--decimate")==0) dodecimate=!dodecimate;
    }
  }
  if(DoExit) return(EX_OK);
  
  /* Datum holen fuer filenamen */
  date=vs_date();
  if(dosyslog) {
    sysfilename=malloc(strlen(SYSFILE)+16);
    sprintf(sysfilename,SYSFILE,date);
    sysdptr=fopen(sysfilename,"a");
    if(!sysdptr) {
      printf("Opening Syslog file %s failed.\nWill not syslog! Continue...\n",sysfilename);
      dosyslog=0;
    } else {
      char *times=vs_time();
      fprintf(sysdptr,"###\n### TTTracklog V." TTTracklogVERSION " started at %s %s\n###\n",date,times);
      free(times);
    }
  } 
  /* Feed oeffnen */
  feedptr=fopen(gpsdevice,"r");
  if(!feedptr) io_error(errno,"OPEN gpsdevice");
  if(doclean) {
   int fp,i,c=0;
    /* Erstmal alles lesen was da kommt, damit wir an das 
       Ende des Buffers kommen */
    fp=fileno(feedptr);
    ioctl(fp, FIONREAD, &i);
    while(i) {
     if(verbose) printf("read... i=%d\n",i);
      c+=fread(nmeabuffer,1,min(i,BUFSIZE),feedptr);
      ioctl(fp, FIONREAD, &i);
      if(verbose) printf("c=%d\n",c);
    }  
    if(verbose) printf("fertig.\n");
  }

  /* timer merken */
  t=time(NULL);
  gpxfilename=malloc(strlen(GPXFILE)+16+strlen(datapath));
  itnfilename=malloc(strlen(ITNFILE)+16);
  logfilename=malloc(strlen(LOGFILE)+16+strlen(datapath));
  sprintf(gpxfilename,GPXFILE,datapath,date);
  sprintf(itnfilename,ITNFILE,date);
  sprintf(logfilename,LOGFILE,datapath,date);

if(USE_RAM) {
  track=malloc(sizeof(TRACKPOINT)*MAX_TRACK);
  if(dosyslog && verbose) fprintf(sysdptr,"Memory usage: %d Bytes, %dx%d.\n",
     sizeof(TRACKPOINT)*MAX_TRACK,sizeof(TRACKPOINT),MAX_TRACK);

  /* Here we catch an error, which occured in firmware Version 8.2 */
  if(track==NULL) {
    int i;
    if(dosyslog && verbose>=0) 
      fprintf(sysdptr,"MALLOC() failed! This could mean, that there is not"
       "enough memory available on your device! We asked for %d Bytes.\n",
       sizeof(TRACKPOINT)*MAX_TRACK);
      for(i=MAX_TRACK;i>1000;) {
         i>>=1;
         if(dosyslog && verbose>=0) fprintf(sysdptr,"Try again with %d Bytes.\n",sizeof(TRACKPOINT)*i);
         track=malloc(sizeof(TRACKPOINT)*i);
	 if(track!=NULL) break;
      }
      if(track==NULL) {
         if(dosyslog && verbose>=0) 
	   fprintf(sysdptr,"The problem persists. Sorry. It makes no sense to allocate space for less than 10000 Trackpoints.\nQUIT.\n");
           if(dosyslog) fclose(sysdptr);
	   return(EX_UNAVAILABLE);
      } else {
         if(dosyslog && verbose>=0) fprintf(sysdptr,"OK, we now have space for %d trackpoints.\n" 
	 "This situation is not satisfactory, but may work for a while. TTTracklog may crash at the moment when more than %d " 
	 "trackpoints where recorded per day. This is normally after %5.5g hours. If you can live with that, its OK.\n",i,i,((double)i)/60/60);
      }
    }
  
    /* Check if a logfile for that day already exist */
  
    if(exist(logfilename)) {
      load_logfile(logfilename);
      if(dosyslog && verbose>=0) fprintf(sysdptr,"<-- %s : %d Trackpoints.\n",logfilename,anztrack);
    }  
  }
  /* Make sure that on termination (by a signal) the track gets written to the files */
  
  signal(SIGINT, break_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGHUP, signal_handler);

  if(dosyslog && verbose>=0) {
    fprintf(sysdptr,"NMEA logging started [");
    fflush(sysdptr);
  }
  
  for(;;) {
    /* Daten in Buffer lesen */
    bufpointer+=fread(&nmeabuffer[bufpointer],1,min(64,BUFSIZE-bufpointer),feedptr);
    if(verbose) printf("%03d: Buffer-fill: %d/%d\n",(int)time(NULL)-t,bufpointer,BUFSIZE);
    if(dosyslog && verbose) fprintf(sysdptr,".");

    /* Wenn buffer voll oder eine Viertelstunde rum ist*/
    if(bufpointer>=BUFSIZE || time(NULL)-t>1000) {
      char *date2;

      process_log();

      if(doitn && anztrack) save_itnfile(itnfilename); /* itn rausschreiben */
      if(dosyslog && verbose>=0) {
        fprintf(sysdptr," (%d) ",anztrack);
        fflush(sysdptr);
      }


      date2=vs_date();  /* Datum holen */
      if(strcmp(date,date2)!=0) { /* hat sich was geaendert ? */
        if(USE_RAM) get_stats();
        if(dogpx && anztrack) save_gpxfile(gpxfilename); /* gpx rausschreiben */
     /* Hier nicht mehr noetig...
        if(doitn && anztrack) save_itnfile(itnfilename);  itn rausschreiben */
      
        if(dosyslog) print_statistik(sysdptr);
	anztrack=0;
	strcpy(date,date2);
        sprintf(gpxfilename,GPXFILE,datapath,date);
        sprintf(itnfilename,ITNFILE,date);
        sprintf(logfilename,LOGFILE,datapath,date);
	if(dosyslog && verbose>=0) fprintf(sysdptr,"]\n New day: %s\n[",date);
      }
      free(date2);
    
      /* timer merken */
      t=time(NULL);    
    }
  }
  if(dosyslog && verbose>=0) fprintf(sysdptr,"]\nlogging stopped.\n");
  fclose(feedptr);
  process_log();  
  if(USE_RAM) get_stats();
  if(dogpx && anztrack) save_gpxfile(gpxfilename);
  if(doitn && anztrack) save_itnfile(itnfilename);
  if(dosyslog) {print_statistik(sysdptr);fclose(sysdptr);}
  free(gpxfilename);
  free(itnfilename);
  free(logfilename);
  free(date);  
  if(USE_RAM) free(track);
  return(EX_OK);
}
