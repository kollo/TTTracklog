/* nmea module konvertiert nmea-log files in brauchbare itn Routen-Files fuer TomTom  */
/*(c) Markus Hoffmann 2008-2010 */


/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "TTTracklog.h"
#include "nmea.h"
#include "geo.h"
#include "io.h"

extern TRACKPOINT *track;
extern int anztrack;

extern int verbose;

double nmea_toursep=16;  /* in seconds */
int altitude_correction_typ; /* 1=auto, 2=geoid 4=manual */
double altitude_correction=0; /* in meters */

int wort_sep (char *t,char c,int klamb ,char *w1, char *w2)    {
  int f=0, klam=0;
  if(!(*t)) return(*w1=*w2=0);
  while(*t && (*t!=c || f || klam>0)) {
    if(*t=='"') f=!f;
    else if(!f && (((klamb&1) && *t=='(') || ((klamb&2) && *t=='[') || ((klamb&4) && *t=='{'))) klam++;
    else if(!f && (((klamb&1) && *t==')') || ((klamb&2) && *t==']') || ((klamb&4) && *t=='}'))) klam--;
    *w1++=*t++;
  }
  if(!(*t)) {*w2=*w1=0;return(1);} else {*w1=0;strcpy(w2,++t);return(2);}
}

/* Macht einen Unix-Timestamp aus den GPS-Zeitinformationen. 
Da diese sich auf GMT (=UTC-00) beziehen, sollte timegm() verwendet werden 
und nicht mktime() */

time_t timestamp(int D,int M,int J,int h, int m,int s) {
  struct tm loctim;
  loctim.tm_sec=s;
  loctim.tm_min=m;
  loctim.tm_hour=h;
  loctim.tm_mday=D;
  loctim.tm_mon=M;
  loctim.tm_year=J-1900;
#ifdef WINDOWS
  return(mktime(&loctim));
#else
  return(timegm(&loctim)); /* Achtung: Timegm ist nicht portabel */
#endif
}

char *vs_time() {
  char *ergebnis;
  time_t timec;
  struct tm * loctim;
  timec = time(&timec);
  ergebnis=malloc(9);
  loctim=localtime(&timec);
  strncpy(ergebnis,ctime(&timec)+11,8);
  ergebnis[8]=0;
  return(ergebnis);
}
char *vs_date() {
  char *ergebnis;
  time_t timec;
  struct tm * loctim;
  ergebnis=malloc(12);
  timec = time(&timec);
  loctim=localtime(&timec);
  sprintf(ergebnis,"%04d-%02d-%02d",1900+loctim->tm_year,loctim->tm_mon+1,loctim->tm_mday);
  return(ergebnis);
}
char *vs_time2(time_t timec) {
  char *ergebnis;
  ergebnis=malloc(9);
  strncpy(ergebnis,ctime(&timec)+11,8);
  ergebnis[8]=0;
  return(ergebnis);
}
char *vs_date2(time_t timec) {
  char *ergebnis;
  struct tm * loctim;
  ergebnis=malloc(12);
  loctim=localtime(&timec);
  sprintf(ergebnis,"%04d-%02d-%02d",1900+loctim->tm_year,loctim->tm_mon+1,loctim->tm_mday);
  return(ergebnis);
}

unsigned char chk(char *s) {
  unsigned char c=0;
  while(*s) c^=*s++;
  return(c);
}
void processGPGSA(char *n) {
  char w1[strlen(n)+1],w2[strlen(n)+1];
  int e,i=0;
  if(verbose>1) printf("GPGSA:");
  e=wort_sep(n,',',TRUE,w1,w2);
  while(e>0) {
    if(i==0) track[anztrack].mode=*w1;
    else if(i==1) track[anztrack].fix=(short)atof(w1);
    else if(i>=2 && i<14) {  ;  /* Satellite numbers */
    } else if(i==14) track[anztrack].pdop=atof(w1);
    else if(i==15) track[anztrack].hdop=atof(w1);
    else if(i==16) track[anztrack].vdop=atof(w1);
    else printf("%d:%s\n",i,w1);
    i++;
    e=wort_sep(w2,',',TRUE,w1,w2);
  }
}
void processGPGSV(char *n) {
#if 0
  char w1[strlen(n)+1],w2[strlen(n)+1];
  int e;
  e=wort_sep(n,',',TRUE,w1,w2);
  if(verbose>2) printf("GPGSV:");
  while(e>0) {
    //printf("%s\n",w1);
    e=wort_sep(w2,',',TRUE,w1,w2);
  }
#endif
}
void processPGLOR(char *n) {
#if 0
  char w1[strlen(n)+1],w2[strlen(n)+1];
  int e=wort_sep(n,',',TRUE,w1,w2);
  if(verbose>2) printf("PGLOR:");
  while(e>0) {
   // printf("%s\n",w1);
    e=wort_sep(w2,',',TRUE,w1,w2);
  }
//  printf("PGLOR Time=%g\n",track[anztrack].time);
#endif
#if 0
  track[anztrack].flags|=FLAG_PGLOR;
#endif
}
void processGPGGA(char *n) {
  char w1[strlen(n)+1],w2[strlen(n)+1];
  int e,i=0;
  double d,f;
  int h,m;
  double s;
  if(verbose>1) printf("GPGGA:");
  e=wort_sep(n,',',TRUE,w1,w2);
  while(e>0) {
    if(i==0) {
      f=d=atof(w1);
      h=(int)(d/10000);
      d-=h*10000;      
      m=(int)(d/100);
      d-=m*100;      
      s=d;
      if(track[anztrack].time!=f) anztrack++;
      track[anztrack].time=f;
      track[anztrack].timestamp=s+m*60.0+h*60*60;
    } else if(i==1) {
      d=atof(w1);
      h=(int)(d/100);
      d-=(double)h*100;      
      track[anztrack].lat=(double)h+d/60;
    } else if(i==2) {
      if(*w1=='S') track[anztrack].lat=-track[anztrack].lat;
    } else if(i==3) {
      d=atof(w1);
      h=(int)(d/100);
      d-=h*100;
      track[anztrack].lon=(double)h+d/60;
    } else if(i==4) {
      if(*w1=='W') track[anztrack].lon=-track[anztrack].lon;
    } else if(i==5) track[anztrack].quality=atof(w1);
    else if(i==6) track[anztrack].numsat=(unsigned short)atof(w1);
    else if(i==7) track[anztrack].dilution=atof(w1); /* horizontal dilution of precision, 0.5 -- 99.9*/
    else if(i==8) track[anztrack].alt=atof(w1); /* Altitude (sea level)*/
    else if(i==9) /*printf("Alt: %s\n",w1)*/; /* Unit of altitude m */
    else if(i==10) track[anztrack].geosep=atof(w1); /* Geoid separation alt*/
    else if(i==11) /*printf("Geosep: %s\n",w1)*/;/* Unit of altitude m */
    else if(i==12) track[anztrack].dgpsalter=atof(w1);
    else if(i==13) track[anztrack].dgpsid=(unsigned short)atof(w1); /* ID of DGPS Reference Station */
    else printf("%d:%s\n",i,w1);    
    i++;
    e=wort_sep(w2,',',TRUE,w1,w2);
  }
  /* Altitude correction, if required */
  if(altitude_correction_typ&1) track[anztrack].alt-=track[anztrack].geosep;
  if(altitude_correction_typ&4) track[anztrack].alt-=altitude_correction;
  if(altitude_correction_typ&2) track[anztrack].alt-=wgs84_to_msl_delta(track[anztrack].lat,track[anztrack].lon);
}
void processGPRMC(char *n) {
  char w1[strlen(n)+1],w2[strlen(n)+1];
  int e,i=0;
  double d,f;
  int h;
  if(verbose>1) printf("GPRMC:");
  e=wort_sep(n,',',TRUE,w1,w2);

  while(e>0) {
    if(i==0) {
      f=atof(w1);
      if(track[anztrack].time!=f) anztrack++;
      track[anztrack].time=f;
    } else if(i==1) track[anztrack].status=*w1;
    else if(i==2) {
      d=atof(w1);
      h=(int)(d/100);
      d-=h*100;      
      track[anztrack].lat=(double)h+d/60;
    } else if(i==3) {
      if(*w1=='S') track[anztrack].lat=-track[anztrack].lat;
    } else if(i==4) {
      d=atof(w1);
      h=(int)(d/100);
      d-=h*100;      
      track[anztrack].lon=(double)h+d/60;
    } else if(i==5) {
      if(*w1=='W') track[anztrack].lon=-track[anztrack].lon;
    } else if(i==6) track[anztrack].speed=atof(w1);
    else if(i==7) track[anztrack].kurs=atof(w1);
    else if(i==8) {
      int D,M,J,h,m,s;
      d=track[anztrack].time;
      h=(int)(d/10000);d-=h*10000;
      m=(int)(d/100);d-=m*100; 
      s=(int)d;d-=s;
      f=track[anztrack].date=atof(w1);
      D=(int)(f/10000);f-=D*10000;
      M=(int)(f/100);f-=M*100;
      J=(int)f+2000;

      track[anztrack].timestamp=(double)timestamp(D,M-1,J,h,m,s)+d;
#if 1
    /* Hier kann man nachschauen, ob das Geraet ausgeschaltet war... */
    if(track[anztrack].timestamp>24*60*60) {
      static double lasttimestamp=0;
      double diff=track[anztrack].timestamp-lasttimestamp;
      if(diff>nmea_toursep) {
     //   printf("\nDIFF=%g sek.  %d\n",diff,anztrack);
        if(lasttimestamp) track[anztrack].flags|=FLAG_PGLOR;
      }
      lasttimestamp=track[anztrack].timestamp;
    } 
#endif
    } 
    else if(i==9) track[anztrack].magdev=atof(w1);  /*  magdev */
    else if(i==10) {
      if(*w1=='E') track[anztrack].magdev=-track[anztrack].magdev;  /*   magdev direction */
    } else if(i==11) track[anztrack].mode2=*w1;
    else printf("%d:%s\n",i,w1);
    i++;
    e=wort_sep(w2,',',TRUE,w1,w2);
  }
//  printf("%d:[%02d:%02d:%g] status=%c Lat=%g Lon=%g v=%g k=%g \n",anztrack,h,m,s,track[anztrack].status,
//  track[anztrack].lat,track[anztrack].lon,track[anztrack].speed,track[anztrack].kurs);
}
void nmea_process(char *n) {
  char w1[strlen(n)+1],w2[strlen(n)+1];
  int e;
  e=wort_sep(n,',',TRUE,w1,w2);
  if(strstr(w1,"$GPGSA")) processGPGSA(w2);
  else if(strstr(w1,"$GPGSV")) processGPGSV(w2);
  else if(strstr(w1,"$GPGGA")) processGPGGA(w2);
  else if(strstr(w1,"$GPRMC")) processGPRMC(w2);
  else if(strstr(w1,"$PGLOR")) processPGLOR(w2);
  else {
    if(verbose>1) printf("\nWARNING: Unknown NMEA: <%s>",n);
  }
}

unsigned int atohex(char *p) {
  unsigned int cc=0;
  unsigned char c;
  
  while((c=*p++)) {
    cc<<=4;
    if(c>='0' && c<='9') c-='0';
    else if(c>='A' && c<='F') c-='A'-10;
    else if(c>='a' && c<='f') c-='a'-10;
    else c=0;
    cc+=c;
  }
  return(cc);
}

void nmea_processline(char *n) {
  char line[strlen(n)+1];
  unsigned char c,cc;
  char *p,*line2;

  strcpy(line,n);
  if(line[strlen(line)-1]=='\r') line[strlen(line)-1]=0;
  if(*line=='$') {
    p=strrchr(line,'*');
    if(p!=NULL) {
      *p++=0;
      c=chk(line+1);
      cc=atohex(p);
      if(c==cc) nmea_process(line);
      else {
        if(verbose>0) printf("\nCHK-ERROR: <%s> %02x %02x ",line,c,cc);
        /* Versuche ggf korrekten Teil der Zeile zu retten: */
        line2=strrchr(line+1,'$');
        if(line2!=NULL) {
	  if(verbose>0) printf("\nCorrection: %s\n",line2);
	  if(cc==chk(line2+1)) nmea_process(line2);
          else {
	    if(verbose>0) printf("failed.\n");
	  }
        }
      }
    }
  } else if(*line=='#') ;  /* Kommentare ignorieren */
  else { 
    if(verbose>0) printf("\nFORMAT-ERROR: <%s> ",line);
  }
}
void load_logfile(char *logfilename) {
  FILE *dptr;
  char line[256];

  if(verbose>0) printf("<-- %s [",logfilename);
  dptr=fopen(logfilename,"r");
  while(!myeof(dptr)) {
    lineinput(dptr,line);
    nmea_processline(line);
  }
  if(verbose>0) printf("] %d trackpoints.\n",anztrack);
  fclose(dptr);
}
