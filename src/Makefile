# Makefile for TomTomTracklog V.1.14 (c) Markus Hoffmann 2008-2010 #

# This file is part of TTTracklog, the TomTom GPS Track logger 
# ======================================================================
# TTTracklog is free software and comes with NO WARRANTY - read the file
# COPYING for details
#


# This is the path for compilation without floating point emulation
# PATH=/usr/bin:/usr/local/armlinux/3.3.3/bin:/bin

# This is the path to the original TomTom development environment
# PATH=/usr/bin:/usr/local/arm-linux/bin:/bin

# Directories
prefix=/usr
exec_prefix=${prefix}

BINDIR=${exec_prefix}/bin
DATADIR=${prefix}/share
LIBDIR=${exec_prefix}/lib
MANDIR=${prefix}/share/man
LIBNO=1.14
RELEASE=3

all: TTTracklog bindist log2itn log2itn-l whereami log2itn-$(LIBNO)-win.zip
local: log2itn-l whereami-l

# Cross-Compiler fuer Windows-Excecutable
# WINCC=i386-mingw32msvc-gcc
WINCC=i686-w64-mingw32-gcc

# Cross-Compiler fuer ARM-Linux-Excecutable
CC = arm-linux-gcc
DEBUG_DEFINES = -Wall -O3
STRIP = arm-linux-strip

OBJS= TTTracklog.o gpx.o itn.o nmea.o geo.o alt.o io.o

CSRC=$(OBJS:.o=.c) whereami.c log2itn.c
HSRC= TTTracklog.h gpx.h itn.h geo.h io.h
DIST= README INSTALL COPYING AUTHORS RELEASE_NOTES Makefile $(CSRC) $(HSRC) \
      ../doc/TTTracklog-$(LIBNO).pdf ../doc/TTTracklog-Discussion-$(LIBNO).pdf TTTracklog.cap \
      TTTracklog-wrapper track.fig track.bmp ../doc/log2itn-$(LIBNO).pdf\
      ../doc/whereami-1.12.pdf ../doc/man-pages/log2itn.1
BINDIST= README RELEASE_NOTES TTTracklog TTTracklog.cap TTTracklog-wrapper \
      track.bmp whereami log2itn
EXTDIST= ../contrib/track.bmp ../contrib/AUTHORS ../contrib/TTTracklog-stop \
	../contrib/track-stop.bmp ../contrib/TTTracklog.cap \
	../contrib/TTTracklog-stop.cap ../contrib/whereami.bmp \
	../contrib/whereami.cap ../contrib/whereami-wrapper \
	../contrib/log2itn-wrapper ../contrib/log2itn.cap \
	../contrib/log2itn.bmp

INCS = -I. -I$(HOME)/TomTom/ttlinux/include


DIR=TTTracklog-$(LIBNO)
DIR2=$(DIR)-bin
DIR3=$(DIR)-ext

DISTOBJS=$(OBJS)
DIST2=$(DISTOBJS:.o=.c)
DEPSRC=$(DISTOBJS:.o=.c)

.c.o:
	$(CC) $(DEBUG_DEFINES) -c -DLINUX_TARGET -DBARCELONA_TARGET $(INCS) $*.c

TTTracklog: $(OBJS) Makefile
	arm-linux-gcc -o $@ $(OBJS) -lm 
#	/usr/local/armlinux/3.3.3/arm-linux/lib/libutil.a
	$(STRIP) $@
TTTracklog-l: TTTracklog.c itn.c itn.h gpx.c gpx.h nmea.h nmea.c geo.c geo.h alt.c io.c  Makefile
	gcc -o $@ TTTracklog.c itn.c gpx.c nmea.c geo.c alt.c io.c -lm 
	strip $@
log2itn-l: log2itn.c itn.c itn.h gpx.c gpx.h nmea.h nmea.c geo.c geo.h alt.c io.c Makefile
	gcc -o $@ log2itn.c itn.c gpx.c nmea.c geo.c alt.c io.c -lm 
log2itn: log2itn.o itn.o itn.h gpx.o gpx.h nmea.h nmea.o geo.o geo.h alt.o io.o
	arm-linux-gcc -o $@ log2itn.o itn.o gpx.o nmea.o geo.o alt.o io.o -lm 
	$(STRIP) $@
log2itn.exe: log2itn.c itn.c itn.h gpx.c gpx.h nmea.h nmea.c geo.c geo.h alt.c io.c Makefile
	$(WINCC) -DWINDOWS  -o $@ log2itn.c itn.c gpx.c nmea.c geo.c alt.c io.c -lm 
	strip $@
log2itn-$(LIBNO)-win.zip : log2itn.exe log2itn.bat readme-l2i.txt
	zip -r $@ log2itn.exe log2itn.bat readme-l2i.txt

whereami-l: whereami.c io.c Makefile
	gcc -o $@ -DFAKE whereami.c io.c -lm 
	strip $@
whereami: whereami.o io.o
	arm-linux-gcc -o $@ whereami.o io.o 
	$(STRIP) $@
track.bmp: track.png
	convert -scale 64 $< $@
clean:
	rm -f backup-*.tgz *.o *.bak *~ 
distclean : clean
	rm -f *.so *.a *.itn *.gpx TTTracklog *-bin.zip *-ext.zip log2itn-l \
	log2itn log2itn.exe whereami whereami-l
	rm -rf doc-pak
dist :	$(DIR).tar.gz

log2itn-win: log2itn.exe
	zip log2itn-win-1.13-1.zip log2itn.exe

$(DIR).tar.gz : $(DIST2)
	rm -rf /tmp/$(DIR)
	mkdir /tmp/$(DIR)
	(tar cf - $(DIST))|(cd /tmp/$(DIR); tar xpf -)
	(cd /tmp; tar cf - $(DIR)|gzip -9 > $(DIR).tar.gz)
	mv /tmp/$(DIR).tar.gz .
bindist : $(DIR2).zip

extensions : $(DIR3).zip

$(DIR2).zip : $(BINDIST)
	rm -rf /tmp/$(DIR2)
	mkdir /tmp/$(DIR2)
	mkdir /tmp/$(DIR2)/bin
	mkdir /tmp/$(DIR2)/SDKRegistry
	rsync -a README /tmp/$(DIR2)/
	rsync -a RELEASE_NOTES /tmp/$(DIR2)/
	rsync -a whereami /tmp/$(DIR2)/bin/
	rsync -a log2itn /tmp/$(DIR2)/bin/
	rsync -a TTTracklog /tmp/$(DIR2)/bin/
	rsync -a TTTracklog-wrapper /tmp/$(DIR2)/bin/
	rsync -a track.bmp /tmp/$(DIR2)/SDKRegistry/
	rsync -a TTTracklog.cap /tmp/$(DIR2)/SDKRegistry/
	mkdir /tmp/$(DIR2)/statdata/
	mkdir /tmp/$(DIR2)/itn/
	mkdir /tmp/$(DIR2)/text/
	(cd /tmp ; zip -r $(DIR2).zip $(DIR2))
	mv /tmp/$(DIR2).zip .	
$(DIR3).zip : $(EXTDIST)
	rm -rf /tmp/$(DIR3)
	mkdir /tmp/$(DIR3)
	mkdir /tmp/$(DIR3)/bin
	mkdir /tmp/$(DIR3)/SDKRegistry
#	rsync -a README /tmp/$(DIR3)/
	rsync -a ../contrib/AUTHORS /tmp/$(DIR3)/
#	rsync -a RELEASE_NOTES /tmp/$(DIR3)/
#	rsync -a whereami /tmp/$(DIR3)/bin/
#	rsync -a log2itn /tmp/$(DIR3)/bin/
#	rsync -a TTTracklog /tmp/$(DIR3)/bin/
	rsync -a ../contrib/log2itn-wrapper /tmp/$(DIR3)/bin/
	rsync -a ../contrib/log2itn.bmp /tmp/$(DIR3)/SDKRegistry/
	rsync -a ../contrib/log2itn.cap /tmp/$(DIR3)/SDKRegistry/
	rsync -a ../contrib/whereami-wrapper /tmp/$(DIR3)/bin/
	rsync -a ../contrib/TTTracklog-stop /tmp/$(DIR3)/bin/
	rsync -a ../contrib/whereami.bmp /tmp/$(DIR3)/SDKRegistry/
	rsync -a ../contrib/whereami.cap /tmp/$(DIR3)/SDKRegistry/
	rsync -a ../contrib/track-start.bmp /tmp/$(DIR3)/SDKRegistry/track.bmp
	rsync -a ../contrib/track-stop.bmp /tmp/$(DIR3)/SDKRegistry/
	rsync -a ../contrib/TTTracklog.cap /tmp/$(DIR3)/SDKRegistry/
	rsync -a ../contrib/TTTracklog-stop.cap /tmp/$(DIR3)/SDKRegistry/
#	mkdir /tmp/$(DIR3)/statdata/
#	mkdir /tmp/$(DIR3)/itn/
#	mkdir /tmp/$(DIR3)/text/
	(cd /tmp ; zip -r $(DIR3).zip $(DIR3))
	mv /tmp/$(DIR3).zip .
install : log2itn-l ../doc/man-pages/log2itn.1
	install -s -m 755  log2itn-l $(BINDIR)/log2itn
	install -m 644  ../doc/man-pages/log2itn.1 $(MANDIR)/man1/
uninstall :
	rm -f $(BINDIR)/log2itn
	rm -f $(MANDIR)/man1/log2itn.1
doc-pak: ../AUTHORS ../COPYING  ../README ../RELEASE_NOTES ../TODO copyright changelog.Debian
	mkdir -p $@
	cp $+ $@/
	gzip -n -9 $@/changelog.Debian
deb :	log2itn-l ../doc/man-pages/log2itn.1 doc-pak
	sudo checkinstall -D --pkgname log2itn --pkgversion $(LIBNO) \
	--maintainer kollo@users.sourceforge.net --pkgrelease $(RELEASE) \
	--provides log2itn --backup --pkggroup tools \
	--pkglicense GPL --strip=yes --stripso=yes --reset-uids
	sudo rm -r backup*.tgz
	sudo chown 1000 log2itn_$(LIBNO)-$(RELEASE)_*.deb
depend : $(DEPSRC) 
	cp Makefile Makefile.bak
	chmod +w Makefile
	makedepend $(INC) $(DEF) $(DEPSRC) 
	sed -n '/^# DO NOT DELETE/,$$p' < Makefile > Makefile.tmp
	sed -n '1,/^# DO NOT DELETE/p' < Makefile > Makefile.new
	tail +2 Makefile.tmp|\
	sed 's/^\([A-Za-z0-9_]*\)\.o:/\1.o \1.ln:/g'>>Makefile.new
	rm -f Makefile.tmp
	mv Makefile.new Makefile

# DO NOT DELETE
