/* log2itn    konvertiert nmea-log files in brauchbare itn Routen-Files fuer TomTom  */
/*(c) Markus Hoffmann 2008 */


/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#include <stdlib.h>
#include <stdio.h>

#include <string.h>
#include <unistd.h>
#ifndef WINDOWS
#include <sysexits.h>
#endif
#include <sys/time.h>
//#include <math.h>

#include "TTTracklog.h"
#include "itn.h"
#include "gpx.h"
#include "nmea.h"
#include "io.h"

#ifdef WINDOWS
#define EX_USAGE 0
#define EX_OK 0
#define EX_NOINPUT 0
#define EX_UNAVAILABLE 0
#endif

extern double itn_wayptsep;
extern double itn_wayptsep2;
extern double itn_wayptdist;
extern double itn_wayptdist2;
extern double itn_wayptdeg;
extern double itn_wayptdeg2;
extern double itn_halttime;
extern int itn_localtime;
extern double nmea_toursep;

extern int altitude_correction_typ;
extern double altitude_correction;

TRACKPOINT *track;
int anztrack=0;

int verbose=1;

char logfilename[256]="/mnt/sdcard/statdata/nmea-11.11.2008.log";
char itnfilename[256]="a.itn";
char gpxfilename[256]="a.gpx";
char altfilename[256]="a.alt";

char *date;

void intro() { 
  puts("log2itn   (c) 2008-2009 Markus Hoffmann");
}
void usage() { 
  puts("\nUsage: "
       " log2itn [options] <file.log> --- convert nmea log to itn files \n");
  puts(  "-h --help         --- Show usage");
  printf("-o <filename.itn> --- place itn output to file [%s]\n",itnfilename);
  printf("-g <filename.gpx> --- place gpx output to file [%s]\n",gpxfilename);
  printf("-a <filename.alt> --- place altitude output to file [%s]\n",altfilename);
  puts(  "--aac             --- use altitude auto correction\n"
         "--amc <value>     --- use manual altitude correction, value in meters [0]\n"
         "--agc             --- use altitude correction by simple geoid model\n"
         "--gi <seconds>[,<seconds>] --- set gpx trackpoint interval         [0] seconds\n"
         "--gm <meters>[,<meters>]   --- set minimum gpx trackpoint distance [0] meters\n"
         "--gc <degrees>[,<degrees>] --- set minimal gpx course difference   [0] degrees\n"
         "--gh <seconds>    --- set gpx tracksep halt time  [2] seconds\n"
         "--gnt             --- set local time as trackpoint name\n"
         "--gnd             --- set tour distance as trackpoint name\n"
         "--gnn             --- set point number as trackpoint name\n"
         "--gwp             --- also save waypoints into gpx file\n"
         "--ii <minutes>[,<minutes>] --- set minimum (and maximum) itn waypoint time interval [20] minutes\n"
         "--im <meters>[,<meters>]   --- set minimum (and maximum) itn waypoint distance [100] meters\n"
         "--ic <degrees>[,<degrees>] --- set minimal (and maximal) itn course difference   [0] degrees\n"
         "--ih <seconds>    --- set itn pause time                 [29] seconds\n"
         "--il              --- [don't] use local time (instead of GMT) for data in itn files\n"
         "--ti <seconds>    --- set tour separation time           [16] seconds\n"
         "--decimate        --- Decimate gpx trackpoints \n"
         "--ds <meters>     --- distance significance in meters [5]\n"
         "-v                --- be more verbose\n"
         "-q                --- be more quiet");
}

extern void save_altfile(char *altfilename);

int main(int argc, char** argv) {
  int DoExit=0;
  int e;
  int doalt=0,doitn=0,dogpx=0;
  char w1[100],w2[100];
  if(argc>1) {
    int count;
    for(count=1;count<argc;count++) {    
      if(strcmp(argv[count],"-h")==0 || strcmp(argv[count],"--help")==0) {
        usage();
        DoExit=1;
      } 
      else if(strcmp(argv[count],"-v")==0) verbose++;
      else if(strcmp(argv[count],"-q")==0) verbose--;
      else if(strcmp(argv[count],"-o")==0) {
        count++;
	strcpy(itnfilename,argv[count]);
	doitn=1;
      } else if(strcmp(argv[count],"-g")==0) {
        count++;
	strcpy(gpxfilename,argv[count]);
        dogpx=1;
      } else if(strcmp(argv[count],"--gi")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	gpx_trackptsep=atof(w1);
	if(e==2) gpx_trackptsep2=atof(w2);
      } else if(strcmp(argv[count],"--gm")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	gpx_trackptdist=atof(w1);
	if(e==2) gpx_trackptdist2=atof(w2);
      } else if(strcmp(argv[count],"--gc")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	gpx_trackptdeg=atof(w1);
        if(e==2) gpx_trackptdeg=atof(w2);
      } else if(strcmp(argv[count],"--aac")==0) {
	altitude_correction_typ^=1;
      } else if(strcmp(argv[count],"--agc")==0) {
	altitude_correction_typ^=2;
      } else if(strcmp(argv[count],"--amc")==0) {
	altitude_correction_typ^=4;
	count++;
	altitude_correction=atof(argv[count]);
      } else if(strcmp(argv[count],"--gnt")==0) {
	gpx_name_flags^=1;
      } else if(strcmp(argv[count],"--gnd")==0) {
	gpx_name_flags^=2;
      } else if(strcmp(argv[count],"--gnn")==0) {
	gpx_name_flags^=4;
      } else if(strcmp(argv[count],"--gwp")==0) {
	do_gpxwp=!do_gpxwp;
      } else if(strcmp(argv[count],"--ii")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	itn_wayptsep=atof(w1);
        if(e==2) itn_wayptsep2=atof(w2);
	doitn=1;
      } else if(strcmp(argv[count],"--im")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	itn_wayptdist=atof(w1);
	if(e==2) itn_wayptdist2=atof(w2);
	doitn=1;
      } else if(strcmp(argv[count],"--ih")==0) {
        count++;
	itn_halttime=atof(argv[count]);
	doitn=1;
      } else if(strcmp(argv[count],"--gh")==0) {
        count++;
	gpx_halttime=atof(argv[count]);
      } else if(strcmp(argv[count],"--il")==0) {
        itn_localtime=!itn_localtime;
      } else if(strcmp(argv[count],"--ti")==0) {
        count++;
	nmea_toursep=atof(argv[count]);
      } else if(strcmp(argv[count],"--ds")==0) {
        count++;
	distance_significance=atof(argv[count]);
      } else if(strcmp(argv[count],"--ic")==0) {
        e=wort_sep(argv[++count],',',TRUE,w1,w2);
	itn_wayptdeg=atof(w1);
	if(e==2) itn_wayptdeg2=atof(w2);
	doitn=1;
      } else if(strcmp(argv[count],"--decimate")==0) {
	dodecimate=1;
      } else if(strcmp(argv[count],"-a")==0) {
        count++;
	strcpy(altfilename,argv[count]);
	doalt=1;
      } else {
        strcpy(logfilename,argv[count]);
      }
    }
  } else {
    intro();
    usage();
    return(EX_USAGE);
  }
  if(DoExit) return(EX_OK);
  date=vs_date();
  if(!exist(logfilename)) {
    printf("File %s not found.\n",logfilename);
    exit(EX_NOINPUT);
  } 
  track=malloc(sizeof(TRACKPOINT)*MAX_TRACK);
  /* Here we catch an error, which occured in firmware Version 8.2 */
  if(track==NULL) {
     int i;
     if(verbose>=0) 
       printf("MALLOC() failed! This could mean, that there is not"
       "enough memory available on your device! We asked for %d Bytes.\n",
       (unsigned int)sizeof(TRACKPOINT)*MAX_TRACK);
       for(i=MAX_TRACK;i>1000;) {
         i>>=1;
         if(verbose>=0) printf("Try again with %d Bytes.\n",(unsigned int)sizeof(TRACKPOINT)*i);
         track=malloc(sizeof(TRACKPOINT)*i);
	 if(track!=NULL) break;
       }
       if(track==NULL) {
         if(verbose>=0) 
	   printf("The problem persists. Sorry. It makes no sense to allocate space for less than 10000 Trackpoints.\nQUIT.\n");
	   return(EX_UNAVAILABLE);
       } else {
         if(verbose>=0) printf("OK, we now have space for %d trackpoints.\n" 
	 "This situation is not satisfactory, but may work for a while. TTTracklog may crash at the moment when more than %d " 
	 "trackpoints where recorded per day. This is normally after %5.5g hours. If you can live with that, its OK.\n",i,i,((double)i)/60/60);
       }
  }
  if(verbose>2) printf("Memory usage: %d Bytes, %dx%d.\n",
  (unsigned int)sizeof(TRACKPOINT)*MAX_TRACK,(unsigned int)sizeof(TRACKPOINT),MAX_TRACK);


  load_logfile(logfilename);
  get_stats();  

  if(anztrack && dogpx) save_gpxfile(gpxfilename);
  if(anztrack && doitn) save_itnfile(itnfilename);
  if(anztrack && doalt) save_altfile(altfilename);
  if(verbose>=0) print_statistik(stdout);
  free(track);
  free(date);
  return(EX_OK);
}
