/* io module File etc... fuer TomTom  */
/*(c) Markus Hoffmann 2009 */


/* This file is part of TTTracklog, the TomTom GPS Track logger 
 * ======================================================================
 * TTTracklog is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "io.h"

/* liest eine ganze Zeile aus einem ASCII-File ein 
   Code taken from X11-Basic  */

char *lineinput( FILE *n, char *line) {   
  int c; int i=0;
  while((c=fgetc(n))!=EOF) {
    if(c==(int)'\n') {line[i]=(int)'\0';return line;} else line[i++]=(char)c;
  }
  line[i]='\0';
  return line;
}
int myeof(FILE *n) {int c=fgetc(n);ungetc(c,n);return c==EOF;}


int exist(char *filename) {
  struct stat fstats;
  int retc=stat(filename, &fstats);
  if(retc==-1) return(FALSE);return(TRUE);
}


size_t lof(FILE *n) {	
  size_t laenge,position=ftell(n);
  if(fseek(n,0,2)==0){
    laenge=ftell(n);
    if(fseek(n,position,0)==0)  return(laenge);
  }
  return(-1);
}
